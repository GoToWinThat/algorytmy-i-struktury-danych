﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticTrouble
{
    public class FunctionToSolve
    {
        public double Func(double[] input)
        {
            return (-Math.Floor(input[0] * input[1]) + Math.Pow(input[0], 2) + Math.Pow(input[1], 2));
        }
    }
}
