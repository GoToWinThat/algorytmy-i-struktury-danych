﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticTrouble
{
    class Program
    {
        static void Main(string[] args)
        {
            Genetic Genetic = new Genetic();

            FunctionToSolve functionToSolve = new FunctionToSolve();

            double[] resultWeierstrass = Genetic.Find(functionToSolve.Func, new double[] { -50.0, 50.0 }, 10000, 300, 2, 0.5, 0.5);
            double solutionWeierstrass = functionToSolve.Func(resultWeierstrass);
        }
    }
}
