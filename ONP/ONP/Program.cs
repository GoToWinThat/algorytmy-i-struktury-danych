﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ONP
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Czy chcesz wprowadzić równanie czy sprawdzić test?[1][2]");
            string userInput = Console.ReadLine();

            if (userInput == "2")
            {
                Console.WriteLine("Oto równanie testowe");
               // string test = "( ( 2 + 3 ) * 3 + ( 4 * 5 ) + ( 6 + 7 * 8 * 9 + 11 ) + 12 * 13 )";
                //string test = "1 * 2 ^ 3 + 4 / 5 - 6";
                string test = "1 + 2 ^ 3 / 4 ^ 5 * 6 + 7 - 8 / 9 + 10";
                Console.WriteLine(test);
                List<string> equation = test.Split(' ').ToList();
                Console.WriteLine("Konwersja na ONP");
                Console.WriteLine(ConvertToONP(equation));
                Console.WriteLine("Konwersja z ONP");
                Console.WriteLine(ConvertFromONP(ConvertToONP(equation)));
            }
            else if(userInput == "1")
            {
                Console.WriteLine("Wprowadź równanie ( KAŻDY ZNAK ODDZIELONY SPACJĄ / BŁĘDNE RÓWNANIE NIE OBSŁUGIWANE POPRAWNIE): ");
                userInput = Console.ReadLine();
                List<string> equation = userInput.Split(' ').ToList();
                Console.WriteLine("Konwersja na ONP");
                Console.WriteLine(ConvertToONP(equation));
                Console.WriteLine("Konwersja z ONP");
                Console.WriteLine(ConvertFromONP(ConvertToONP(equation)));
            }
            Console.ReadKey();
        }
        public static string ConvertToONP(List<string> equation)
        {
            List<string> operators = new List<string>();
            string result = "";

            for (int i = 0; i < equation.Count(); i++)
            {
                switch (equation[i])
                {
                    case "(":
                        operators.Add(equation[i]); // Dodajemy nawias otwierający do listy
                        break;
                    case ")":
                        for (int j = operators.Count() - 1; j >= 0; j--) //Jeżeli zamykamy nawias, przeszukujemy wszystkie zapisane operatory
                        {
                            if (operators[j] != "(") //Dopóki nie spotkamy otwarcia nawiasu
                            {
                                result = result + operators[j] + ' '; //Przenosimy wszystkie operatory do rozwiazania
                                operators.RemoveAt(j);  //Usuwamy wykorzysane operatory
                            }
                        }
                        operators.RemoveAt(operators.Count() - 1); //Pozbywamy się nawiasu "("
                        break;
                    case "+":   //Next
                    case "-":   //Next
                    case "*":   //Next
                    case "/":   //Next
                    case "^":
                        for (int j = operators.Count() - 1; j >= 0; j--) //Przeszukujemy operatory 
                        {
                            if (CheckSign(equation[i]) < CheckSign(operators[j]) || CheckSign(equation[i])== CheckSign(operators[j])) //Jezeli pobrany operator ma priorytet mniejszy od ostatniego dodanego do listy
                            {
                                result = result + operators[j] + ' '; //Dodajemy do rozwiazania zank ze stosu (bo ma mniejszy priorytet)
                                operators.RemoveAt(j);

                            }
                            else break;
                        }
                        operators.Add(equation[i]); //Jezeli nowy znak ma najwyższy priorytet, dodajemy go do listy
                        break;
                    default:
                        result = result + equation[i] + ' '; //Wszystkie liczby wedruja od razu do wyniku
                        break;
                }


            }
            //Sformatowanie wyniku
            for (int j = operators.Count()-1; j >= 0; j--)
            {
                result = result + operators[j] + ' ';
            }

            return result;
        }
        static int CheckSign(string s)
        {
            if (s == "+" || s == "-")
            {
                return 1;
            }
            else if (s == "*" || s == "/") return 2;
            else if (s == "^") return 3;
            else return 0;
        }
        static string ConvertFromONP(string equation)
        {
            string[] signs = equation.Split(' ');
            Stack<string> result = new Stack<string>();
            List<string> operatorsList = new List<string> { "(",")","+","-", "*", "/","^" };
            for (int i = 0; i < signs.Length-1; i++)
            {
                if (!operatorsList.Contains(signs[i])) //Zbieramy wszystkie liczby na stos
                {
                    result.Push(signs[i]);
                    continue;
                }
                string one = result.Pop();
                string two = result.Pop();
                //Jezeli pobieramy znak musimy zdjąć ze stosu dwie liczby(takze w postaci wiekszego rownania np (2+3) *  3)
                string newItem = $"({two}{signs[i]}{one})";
                //Odkładamy nowa liczbę na stos
                result.Push(newItem);
            }
            return result.Pop();
        }
    }
}
