﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchMax
{
    class Program
    {
        static void Main()
        {
            double[] numbers = new double[] { 1,2,3,4,5,6,8.5,9.342,1241,324,342,12.4,2.9933343,99999,1};
            double max = 0;
            foreach (var item in numbers)
            {
                if (item > max)
                    max = item;
            }
            Console.WriteLine(max);
            Console.ReadKey();
        }
    }
}
