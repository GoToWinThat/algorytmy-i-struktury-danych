﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    //Dla kolejki priorytetowej nie zaimplementowałem wyszukiwania po indeksie oraz po wartości: mechanizm ten sam co w zwykłej kolejce ze względu na cykliczność
    public class PriorityQueue<T>
    {
        private Element<T>[] _values { get; set; }
        private int _first { get; set; }
        private int _last { get; set; }
        private int _size { get; set; }
        private int _counter { get; set; }

        public class Element<K>
        {
            public K value { get; set; }
            public int prio { get; set; }
        }

        public PriorityQueue(int size)
        {
            _values = new Element<T>[size];
            _size = size;
            _first = 0;
            _last = 0;
            _counter = 0;
        }
        public void Enqueue(T data, int priority)
        {
            if (_counter >= _values.Length)
            {
                throw new IndexOutOfRangeException();
            }

            Element<T> toAdd = new Element<T>
            {
                value = data,
                prio = priority
            };

            int i, k;
            bool isCheck = false;
            bool isSwapped = false;

            //Sprawdzamy postać kolejki
            //W związku z tym, że kolejka jest cykliczna, w strukturze tablicy ostatnio dodany element moze być "przed" dodanym najwcześniej np:
            //890012345 - gdzie liczba odpowiada kolejności dodania. W tym przypadku _last=2 a _first=5

            //Przypadek standardowy-kolejka się nie zapętlila w strukturze tablicy
            if(_last>_first)
            {
                //Przeszukujemy tablice po kolei
                for(i=_first; i<_last; i++)
                {
                    //Jezeli priorytet jest wiekszy od aktualnie rozpatrywanego zaczynamy podmiane
                    if(toAdd.prio > _values[i].prio)
                    {
                        // Od miejsza podmiany do końca kolejki
                        var toSwap = _values[i];
                        _values[i] = toAdd;
                        for ( k=i+1; k<=_last;k++)
                        {
                            var temp = _values[k];
                            _values[k] = toSwap;
                            toSwap = temp;
                        }
                        break;
                    }
                    //Jezeli priorytet jest równy od aktualnie rozpatrywanego - ustawiamy flagę(możliwa jest sytaucja w której mamy np.  3 pozycje z tym samym prioytetem, musimy dodać nowy element na końcu)
                    else if(toAdd.prio ==_values[i].prio && isCheck==false)
                    {
                        isCheck = true;
                    }
                    //Zaistnienie warunku z powyższej flagi
                    else if(toAdd.prio < _values[i].prio && isCheck == true)
                    {
                        //Pętla podmiany
                        var toSwap = _values[i];
                        _values[i] = toAdd;
                        for (k = i + 1; k <= _last; k++)
                        {
                            var temp = _values[k];
                            _values[k] = toSwap;
                            toSwap = temp;
                        }
                        break;
                    }
                }
                //Dodatkowy warunek występujący w momencie kiedy dodajemy element na końcu naszej tablicy(problem wynikający z użycia tablicy)
                if(_values[_last]==null)
                {
                    _values[_last] = toAdd;
                }

            }
            //Przypadek zapętlenia się kolejki w strukturze tablicy
            else if(_first>_last)
            {
                //Przeszukujemy pierwszą cześć tablica(początek kolejki), pozostałem warunki/pętle podmiany pozostając bez zmian, jedynie dzielimy je na dwa:
                //Od np miejsca 5 do 10, a potem od 0 do 5 zgodnie z zapętleniem się kolejki w tablicy
                for(i=_first; i<_size; i++)
                {
                    if (toAdd.prio > _values[i].prio)
                    {
                        var toSwap = _values[i];
                        _values[i] = toAdd;
                        for (k = i + 1; k < _size; k++)
                        {
                            var temp = _values[k];
                            _values[k] = toSwap;
                            toSwap = temp;
                        }
                        for(k=0; k<=_last; k++)
                        {
                            var temp = _values[k];
                            _values[k] = toSwap;
                            toSwap = temp;
                        }
                        isSwapped = true;
                        break;
                    }
                    else if (toAdd.prio == _values[i].prio && isCheck == false)
                    {
                        isCheck = true;
                    }
                    else if (toAdd.prio < _values[i].prio && isCheck == true)
                    {
                        var toSwap = _values[i];
                        _values[i] = toAdd;
                        for (k = i + 1; k < _size; k++)
                        {
                            var temp = _values[k];
                            _values[k] = toSwap;
                            toSwap = temp;
                        }
                        for (k = 0; k < _last; k++)
                        {
                            var temp = _values[k];
                            _values[k] = toSwap;
                            toSwap = temp;
                        }
                        //Flaga podamiany
                        isSwapped = true;
                        break;
                    }
                }
                //Jeżeli nie podmieniliśmy liczb w pierwszej części
                if(!isSwapped)
                {
                    //Przeszukujemy tablicę od początku do ostatniego elementu
                    for (i = 0; i < _last; i++)
                    {
                        if (toAdd.prio > _values[i].prio)
                        {
                            var toSwap = _values[i];
                            _values[i] = toAdd;
                            for (k = i + 1; k <= _last; k++)
                            {
                                var temp = _values[k];
                                _values[k] = toSwap;
                                toSwap = temp;
                            }
                            break;
                        }
                        else if (toAdd.prio == _values[i].prio && isCheck == false)
                        {
                            isCheck = true;
                        }
                        else if (toAdd.prio < _values[i].prio && isCheck == true)
                        {
                            var toSwap = _values[i];
                            _values[i] = toAdd;
                            for (k = i + 1; k <= _last; k++)
                            {
                                var temp = _values[k];
                                _values[k] = toSwap;
                                toSwap = temp;
                            }
                            break;
                        }
                    }
                    if (_values[_last] == null)
                    {
                        _values[_last] = toAdd;
                    }

                }
            }
            //Przypadek kiedy dodajemy całkowicie nowy element do pustej kolejki
            else if(_first==_last)
            {
                _values[_last] = toAdd;
            }
            _last = (_last + 1) % _size;
            _counter++;

        }

        public Element<T> Dequeue()
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }

            Element<T> toDel = _values[_first];
            _values[_first] = null;
            _first = (_first + 1) % _size;
            _counter--;
            return toDel;
        }

        public void ShowAll()
        {
            for (int i = 0; i < _size; i++)
            {
                if(_values[i]==null)
                {
                    Console.WriteLine("NULL");
                }
                else
                {
                    Console.WriteLine($"Liczba {_values[i].value} + Priorytet {_values[i].prio}");
                }
            }
        }

        public T First()
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }
            return _values[_first].value;
        }

        public T Last()
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }
            return _values[_last - 1].value;
        }
        public bool IsEmpty()
        {
            if (_counter == 0)
            {
                return true;
            }
            return false;
        }

        public bool IsFull()
        {
            if (_counter == _size)
            {
                return true;
            }
            return false;
        }
        public int Count()
        {
            return _counter;
        }
    }
}
