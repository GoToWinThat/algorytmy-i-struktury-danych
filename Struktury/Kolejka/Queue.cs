﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    public class Queue<T>
    {
        private Element<T>[] _values { get; set; }
        private int _first { get; set; }
        private int _last { get; set; }
        private int _size { get; set; }
        private int _counter { get; set; }
        public class Element<K>
        {
            public K value { get; set; }
        }
        public Queue(int size)
        {
            _values = new Element<T>[size];
            _size = size;
            _first = 0;
            _last = 0;
            _counter = 0;
        }
        
        //Dodaj element do kolejki
        public void Enqueue (T data)
        {
            if(_counter >= _values.Length)
            {
                throw new IndexOutOfRangeException();
            }

            Element<T> toAdd = new Element<T>
            {
                value = data,
            };

            _values[_last] = toAdd;
            _last = (_last+1) % _size;
            _counter++;

        }

        //Usuń element z kolejki
        public T Dequeue()
        {
            if(_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }

            Element<T> toDel = _values[_first];
            _values[_first] = null;
            _first = (_first+1)% _size;
            _counter--;
            return toDel.value;
        }


        public T First()
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }
            return _values[_first].value;
        }

        public T Last()
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }
            return _values[_last-1].value;
        }
        //Metoda ta zwraca nie tyle indeks tabeli co KOLEJNOSC w kolejce wiec argument 4 nie poda nam wartosci czwartej  abeli (czyli o indeksie 3 bo jest numerowana od 0) 
        //a czwarta wartosc w kolejce czyli dla przygotowanego programu wartosc na indeksie 0(pierwsze miejsce w tabeli)
        public T Search_Data(int index)
        {
            if (_counter <= 0 || index >_size)
            {
                throw new IndexOutOfRangeException();
            }
            else
            {
                if (_first + index > _size)
                {
                    return _values[(_first + index-1) - _size].value;
                }
                else
                {
                    return _values[_first+index].value;
                }
            }        
        }

        public int Search_Index(T data)
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }
            else
            {
                if (_first  > _last || (_first==_last && _first>0))
                {
                    for (int i = _first; i < _size; i++)
                    {
                        if (EqualityComparer<T>.Default.Equals(_values[i].value, data))
                        {
                            return i+1 - _first;
                        }
                    }
                    for (int i = 0; i < _last; i++)
                    {
                        if (EqualityComparer<T>.Default.Equals(_values[i].value, data))
                        {
                            return (_size-_first)+i+1;
                        }
                    }
                    throw new NullReferenceException();
                }
                else
                {
                    int loopCondition = _last;
                    if(_last==0)
                    {
                        loopCondition = _size;
                    }
                    for (int i = _first; i < loopCondition; i++)
                    {
                        if (EqualityComparer<T>.Default.Equals(_values[i].value, data))
                        {
                            return (i + 1 - _first);
                        }
                    }
                    throw new NullReferenceException();
                }
            }
        }

        //Metoda wyswietla tabele - nie kolejnosc kolejki
        public void ShowAll()
        {
            for (int i = 0; i < _size; i++)
            {
                if (_values[i] == null)
                {
                    Console.WriteLine("NULL");
                }
                else
                {
                    Console.WriteLine($"Liczba {_values[i].value}");
                }

            }
        }

        public bool IsEmpty()
        {
            if (_counter == 0)
            {
                return true;
            }
            return false;
        }

        public bool IsFull()
        {
            if (_counter == _size)
            {
                return true;
            }
            return false;
        }
        public int Count()
        {
            return _counter;
        }
    }
}
