﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    class DLList_Flag<T>
    {
        private class Element<E>
        {
            public E value { get; set; }
            public Element<E> next = null;
            public Element<E> prev = null;
            public bool isFlag = false;
        }

        private Element<T> _head = null;
        private Element<T> _last = null;
        private Element<T> _flag = null;
        private int _size = 0;

        public DLList_Flag()
        {
            _flag = new Element<T>();
            _flag.value = default;
            _flag.isFlag = true;
            _flag.next = _flag;
            _flag.prev = _flag;
        }

        public void Add(T data)
        {
            Element<T> toAdd = new Element<T>();
            toAdd.value = data;
            if (_head == null)
            {
                _head = toAdd;
                _flag.next = _head;
                _flag.prev = _head;
                _head.next = _flag;
                _head.prev = _flag;
                _size++;
                return;
            }
            else
            {
                toAdd.prev = _flag.prev;
                _flag.prev.next = toAdd;
                _flag.prev = toAdd;
                toAdd.next = _flag; 
                _size++;
                return;
            }
        }
        public void Remove(int index)
        {
            Element<T> curr = _head;
            for (int i = 0; i < index-1; i++)
            {
                if (curr.next == _flag)
                {
                    throw new IndexOutOfRangeException();
                }

                curr = curr.next;
            }
            _size--;
            //Jezeli element nie jest głową 
            if (curr.prev.isFlag != true)
            {
                //To jego poprzedni element wskazuje na element o jeden dalej(nie wskazuje juz na element do usuniecia)
                curr.prev.next = curr.next;
            }

            //Jezeli element nie jest przed flaga
            if (curr.next.isFlag != true)
            {
                //To element przed wskazuje na porzednika ktory jest poprzednikiem usuwanego elementu
                curr.next.prev = curr.prev;
            }

            //Jezeli usuwamy glowe to teraz glowa wskazuje na element jeden dalej
            if (curr == _head)
            {
                _head = curr.next;
            }
        }
        public void RemoveLast()
        {
            Remove(_size);
        }
        public void RemoveFirst()
        {
            Remove(0);
        }

        public T Get(int index)
        {
            Element<T> _toGet = _head;
            for (int i = 0; i < index; i++)
            {
                if (_toGet.next == _flag)
                    throw new IndexOutOfRangeException();

                _toGet = _toGet.next;
            }
            return _toGet.value;
        }
        public int Search(T data)
        {
            Element<T> _toGet = _head;
            int result = 1;
            while(_toGet.isFlag!=true && !EqualityComparer<T>.Default.Equals(_toGet.value, data))
            {
                _toGet = _toGet.next;
                result++;
                if(result>_size)
                {
                    throw new ArgumentNullException();
                }
            }
            return result;
        }

        public void ShowAll()
        {
            if (_size == 0)
            {
                Console.WriteLine("Empty");
            }
            else
            {
                Element<T> _toShow = _head;
                for (int i = 0; i < _size; i++)
                {
                    Console.WriteLine(_toShow.value);
                    _toShow = _toShow.next;
                }
            }
        }
        public int Count()
        {
            return _size;
        }

    }
}


