﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    //Autor: Artur Mazela
    //Sposób użycia:
    //Aby przetestować daną strukturę danych należy odkomentować fragmenty kodu:
    //Każda struktura posiada metody dodawania/usuwania, sprawdzania zapełnienia struktury, podglądania ostatniego/pierwszego elementu (zależnie od jej rodzaju)
    //lub metody dodatkowe służące tylko szybkiego ukazaniu poprawności działania kodu np. w kolejce metoda ShowAll()
    class Program
    {
        //Dodaj/usun - standardowo
        //Dodaj usun podajac wartosc i indeks
        //Szukaj po wartosci i po indeksie
        // Wysiwetl cala liste
        static void Main()
        {
            //Stack:
            /* 
            Stack<int> stack = new Stack<int>(5);

            Console.WriteLine("Stack powinien być pusty i nie pełny: ");
            Console.WriteLine(Convert.ToString(stack.IsEmpty()));
            Console.WriteLine(Convert.ToString(stack.IsFull()));

            Console.WriteLine();

            Console.WriteLine("Stack powinien zawierać na górze po kolei liczby 1,2,3 po wykonaniu metodzie push + first: ");
            stack.Push(1);
            Console.WriteLine(Convert.ToString(stack.First()));
            stack.Push(2);
            Console.WriteLine(Convert.ToString(stack.First()));
            stack.Push(3);
            Console.WriteLine(Convert.ToString(stack.First()));
            Console.WriteLine("Metodą last możemy sprawdzić ostatni element: ");
            Console.WriteLine(Convert.ToString(stack.Last()));

            Console.WriteLine();

            Console.WriteLine("Stack powinien nie być pusty ani pełny: ");
            Console.WriteLine(Convert.ToString(stack.IsEmpty()));
            Console.WriteLine(Convert.ToString(stack.IsFull()));

            Console.WriteLine();

            stack.Push(4);
            Console.WriteLine(Convert.ToString(stack.First()));
            stack.Push(5);
            Console.WriteLine(Convert.ToString(stack.First()));

            Console.WriteLine();

            Console.WriteLine("Stack powinien być nie pusty i pełny: ");
            Console.WriteLine(Convert.ToString(stack.IsEmpty()));
            Console.WriteLine(Convert.ToString(stack.IsFull()));

            Console.WriteLine("Mamy do dyspozycji metody wyszukiwania indeksu po wartości(tylko jednej/pierwszej) oraz uzyskiwania wartości po indeksie:Szukamy po indeksie równym dwa oraz po wartości równej dwa (pojąwią się dwie dwójki) ");
            Console.WriteLine(Convert.ToString(stack.Search_Index(2)));
            Console.WriteLine(Convert.ToString(stack.Search_Data(2)));

            Console.WriteLine();

            Console.WriteLine("Stack powinien być \"czyszczony\", a liczby aktualnie na górze to 4,3 ");
            stack.Pop();
            Console.WriteLine(Convert.ToString(stack.First()));
            stack.Pop();
            Console.WriteLine(Convert.ToString(stack.First()));

            Console.WriteLine();

            Console.WriteLine("Stack powinien nie być pusty ani pełny: ");
            Console.WriteLine(Convert.ToString(stack.IsEmpty()));
            Console.WriteLine(Convert.ToString(stack.IsFull()));

            Console.WriteLine();

            Console.WriteLine("Stack powinien być \"czyszczony\", a liczby aktualnie na górze to 2,1, wyjątek(do odkomentowania w kodzie) ");
            stack.Pop();
            Console.WriteLine(Convert.ToString(stack.First()));
            stack.Pop();
            Console.WriteLine(Convert.ToString(stack.First()));
            stack.Pop();
           // Console.WriteLine(Convert.ToString(stack.Peek()));

            Console.WriteLine();

            Console.WriteLine("Stack powinien być pusty i nie pełny: ");
            Console.WriteLine(Convert.ToString(stack.IsEmpty()));
            Console.WriteLine(Convert.ToString(stack.IsFull()));

            Console.ReadKey();
            */


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //Queue:
            /*
            Queue<int> queue = new Queue<int>(5);

            Console.WriteLine("Queue powinien być pusty i nie pełny: ");
            Console.WriteLine(Convert.ToString(queue.IsEmpty()));
            Console.WriteLine(Convert.ToString(queue.IsFull()));

            Console.WriteLine();

            Console.WriteLine("Queue powiększa się o kolejny element a pierwszym do odczytu/usunięcia jest zawsze pierwszy dodany w kolejności ");
            queue.Enqueue(1);
            Console.WriteLine(Convert.ToString(queue.First())); 
            queue.Enqueue(2);
            Console.WriteLine(Convert.ToString(queue.First()));
            queue.Enqueue(3);
            Console.WriteLine(Convert.ToString(queue.First()));
            Console.WriteLine("Queue powinien być nie pusty i nie pełny: ");
            Console.WriteLine(Convert.ToString(queue.IsEmpty()));
            Console.WriteLine(Convert.ToString(queue.IsFull()));
            queue.Enqueue(4);
            
            Console.WriteLine(Convert.ToString(queue.First()));
            queue.Enqueue(5);
            Console.WriteLine(Convert.ToString(queue.First()));
            Console.WriteLine("Queue powinien być nie pusty i  pełny: ");
            Console.WriteLine(Convert.ToString(queue.IsEmpty()));
            Console.WriteLine(Convert.ToString(queue.IsFull()));

            Console.WriteLine();

            Console.WriteLine("Queue zmiejsza się a pierwszym do odczytu/usunięcia jest zawsze pierwszy dodany w kolejności ");
            queue.Dequeue();

            Console.WriteLine(Convert.ToString(queue.First()));
            queue.Dequeue();
            Console.WriteLine(Convert.ToString(queue.First()));
            queue.Dequeue();
            Console.WriteLine(Convert.ToString(queue.First()));
            queue.Dequeue();
            Console.WriteLine(Convert.ToString(queue.First()));
            queue.Dequeue();

            Console.WriteLine();

            //Sprawdzenie poprawności działania kolejki: cykliczność oraz poprawne dodawanie nowego elemntu od początku tabeli
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Enqueue(4);
            queue.Enqueue(5);
            queue.Dequeue();
            queue.Dequeue();
            queue.Enqueue(6);
            queue.Enqueue(7);
            queue.ShowAll();

            Console.WriteLine("Możemy uzyskąc także element ostatni, oraz wyszukać indeks po wartości lub wartość po indeksie ");
            Console.WriteLine(Convert.ToString(queue.Last()));
            Console.WriteLine(Convert.ToString(queue.Search_Index(6)));
            Console.WriteLine(Convert.ToString(queue.Search_Data(4)));


            Console.ReadKey();
            */

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //PriorityQueue:
            /*
            PriorityQueue<int> priorityQueue = new PriorityQueue<int>(10);


            Console.WriteLine("Queue powiększa się o kolejny element a pierwszym do odczytu/usunięcia jest zawsze element o najwyzszym priorytecie i " +
                "pierwszy w kolejnośc w porównaniu do innych o tym samym priorytecie ");
            priorityQueue.Enqueue(1, 1);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(2, 1);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(3, 2);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(4, 2);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(5, 3);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(6, 3);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(7, 2);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(8, 1);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(9, 2);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Enqueue(10, 3);
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            Console.WriteLine();

            priorityQueue.ShowAll();

            priorityQueue.Dequeue();
            priorityQueue.Enqueue(11, 0);

            Console.WriteLine();

            priorityQueue.ShowAll();
            Console.WriteLine("Ostatni element: ");
            Console.WriteLine(Convert.ToString(priorityQueue.Last()));

            Console.WriteLine();

            Console.WriteLine("Sprawdzenie usuwania z kolejki: ");

            Console.WriteLine();

            priorityQueue.Dequeue();
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Dequeue();
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Dequeue();
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Dequeue();
            Console.WriteLine(Convert.ToString(priorityQueue.First()));
            priorityQueue.Dequeue();
            Console.WriteLine(Convert.ToString(priorityQueue.First()));

            Console.WriteLine();

            priorityQueue.ShowAll();

            priorityQueue.Enqueue(20, 3);
            priorityQueue.Enqueue(30, 1);
            priorityQueue.Enqueue(40, 1);

            Console.WriteLine();

            priorityQueue.ShowAll();

            Console.ReadKey();
            */

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //SLList:
            /*
            SLList<int> sLList = new SLList<int>();
            Console.WriteLine("Dodajemy do listy trzy elementy 1 3 4 a potem element o wartości 2 na drugim miejscu i wyświetlamy listę:");

            sLList.Add(1);
            sLList.Add(3);
            sLList.Add(4);
            sLList.Add(2,2);

            sLList.ShowAll();

            Console.WriteLine("Sprawdzamy jej rozmiar: ");
            Console.WriteLine(Convert.ToString(sLList.Count()));
            Console.WriteLine("Szukamy wartości o indeksie trzecim numerując od 0: ");
            Console.WriteLine(Convert.ToString(sLList.Get(3)));
            Console.WriteLine("Szukamy miejsca na którym znajduje się wartość 4: ");
            Console.WriteLine(Convert.ToString(sLList.Search(4))); 
            Console.WriteLine("Usuwamy wartość pierwszą i ostatnią i wyświetlamy listę oraz jej rozmiar ");
            sLList.RemoveFirst();
            sLList.ShowAll();

            Console.WriteLine();

            sLList.RemoveLast();
            sLList.ShowAll();

            Console.WriteLine();
            Console.WriteLine(Convert.ToString(sLList.Count()));

            Console.ReadKey();
            */

            //DLList:
            /*
            DLList<int> dLList = new DLList<int>();
            Console.WriteLine("Dodajemy do listy trzy elementy 1 3 4 a potem element o wartości 2 na drugim miejscu i wyświetlamy listę:");

            dLList.Add(1);
            dLList.Add(3);
            dLList.Add(4);
            dLList.Add(2, 2);

            dLList.ShowAll();

            Console.WriteLine("Sprawdzamy jej rozmiar: ");
            Console.WriteLine(Convert.ToString(dLList.Count()));
            Console.WriteLine("Szukamy wartości o indeksie trzecim numerując od 0: ");
            Console.WriteLine(Convert.ToString(dLList.Get(3)));
            Console.WriteLine("Szukamy miejsca na którym znajduje się wartość 4: ");
            Console.WriteLine(Convert.ToString(dLList.Search(4)));
            Console.WriteLine("Usuwamy wartość pierwszą i ostatnią i wyświetlamy listę oraz jej rozmiar ");
            dLList.RemoveFirst();
            dLList.ShowAll();

            Console.WriteLine();

            dLList.RemoveLast();
            dLList.ShowAll();

            Console.WriteLine();
            Console.WriteLine(Convert.ToString(dLList.Count()));

            Console.ReadKey();
            */

            //CLList:
            /*
            CLList<int> cLList = new CLList<int>();
            Console.WriteLine("Dodajemy do listy trzy elementy 1 3 4 a potem element o wartości 10 na trzecim miejscu miejscu i wyświetlamy listę:");

            cLList.Add(1);
            cLList.Add(3);
            cLList.Add(4);
            cLList.Add(10, 3);

            cLList.ShowAll();

            Console.WriteLine("Sprawdzamy jej rozmiar: ");
            Console.WriteLine(Convert.ToString(cLList.Count()));
            Console.WriteLine("Szukamy wartości o indeksie trzecim numerując od 0: ");
            Console.WriteLine(Convert.ToString(cLList.Get(3)));
            Console.WriteLine("Szukamy miejsca na którym znajduje się wartość 4: ");
            Console.WriteLine(Convert.ToString(cLList.Search(4)));
            Console.WriteLine("Usuwamy wartość pierwszą i ostatnią i wyświetlamy listę oraz jej rozmiar ");
            cLList.RemoveFirst();
            cLList.ShowAll();

            Console.WriteLine();

            cLList.RemoveLast();
            cLList.ShowAll();

            Console.WriteLine();
            Console.WriteLine(Convert.ToString(cLList.Count()));

            Console.ReadKey();
            */

            //SLList_Flag:
            /*
            DLList_Flag<int> dLList_Flag = new DLList_Flag<int>();
            Console.WriteLine("Dodajemy do listy trzy elementy 1 3 4 a potem element o wartości 10 na trzecim miejscu miejscu i wyświetlamy listę:");

            dLList_Flag.Add(1);
            dLList_Flag.Add(2);
            dLList_Flag.Add(3);
            dLList_Flag.Add(4);

            dLList_Flag.ShowAll();

            Console.WriteLine("Sprawdzamy jej rozmiar: ");
            Console.WriteLine(Convert.ToString(dLList_Flag.Count()));
            Console.WriteLine("Szukamy wartości o indeksie trzecim numerując od 0: ");
            Console.WriteLine(Convert.ToString(dLList_Flag.Get(3)));
            Console.WriteLine("Szukamy miejsca na którym znajduje się wartość 4: ");
            Console.WriteLine(Convert.ToString(dLList_Flag.Search(4)));
            Console.WriteLine("Usuwamy wartość pierwszą i ostatnią i wyświetlamy listę oraz jej rozmiar ");
            dLList_Flag.RemoveFirst();
            dLList_Flag.ShowAll();

            Console.WriteLine();

            dLList_Flag.RemoveLast();
            dLList_Flag.ShowAll();

            Console.WriteLine();
            Console.WriteLine(Convert.ToString(dLList_Flag.Count()));

            Console.ReadKey();
            */
        }
    }
}
