﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    public class Stack<T>
    {
        private T[] _values { get; set; }
        private int _size { get; set; }
        private int _counter { get; set; }

        public Stack(int size)
        {
            _values = new T[size];
            _size = size;
        }

        //Dodaj element
        public void Push(T data)
        {
            if (_counter == _values.Length)
            {
                throw new IndexOutOfRangeException();
            }
            _values[_counter] = data;
            _counter++;
        }

        //Usuń element
        public T Pop()
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }

            T toDel = _values[--_counter];
            _values[_counter] = default(T);
            return toDel;
        }


        //Sprawdź pierwszy element
        public T First()
        {
            if(_counter<=0)
            {
                throw new IndexOutOfRangeException();
            }
            return _values[_counter - 1];
        }

        //Sprawdź ostatni element
        public T Last()
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }
            return _values[0];
        }

        //Szukaj wartości po indeksie
        public T Search_Data(int index)
        {
            if (_counter <= 0 && index > _counter)
            {
                throw new IndexOutOfRangeException();
            }
            return _values[index-1];
        }

        //Szukaj indeksu po wartości
        public int Search_Index(T data)
        {
            if (_counter <= 0)
            {
                throw new IndexOutOfRangeException();
            }

            for(int i=0; i<_counter;i++)
            {
                if (EqualityComparer<T>.Default.Equals(_values[i], data))
                {
                    return i+1;
                }
            }
            return 0;
        }

        //Sprawdza czy stos jest pusty
        public bool IsEmpty()
        {
            if (_counter == 0)
            {
                return true;
            }
            return false;
        }

        //Sprawdza czy stos jest pełny
        public bool IsFull()
        {
            if(_counter==_size)
            {
                return true;
            }
            return false;
        }

        //Zwraca wielkość stosu
        public int Count()
        {
            return _counter;
        }

    }
}
