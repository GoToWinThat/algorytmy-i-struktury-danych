﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    public class CLList<T>
    {
        private class Element<E>
        {
            public E value { get; set; }
            public Element<E> next = null;
        }

        private Element<T> _head = null;
        private Element<T> _last = null;
        private int _size = 0;

        public void Add(T data)
        {
            Element<T> toAdd = new Element<T>();
            toAdd.value = data;
            if (_head == null)
            {
                _head = toAdd;
                _head.next = _head;
                _last = _head;
                _size++;
                return;
            }
            if (_head.next == _head)
            {
                _last = toAdd;
                _head.next = _last;
                _last.next = _head;
                _size++;
                return;
            }
            else
            {
                _last.next = toAdd;
                _last = toAdd;
                _last.next = _head;
                _size++;
                return;
            }
        }

        public void Add(T data, int index)
        {
            if (index > _size)
            {
                throw new IndexOutOfRangeException();
            }
            Element<T> toAdd = new Element<T>();
            toAdd.value = data;
            if (_size == 0 && index == 0)
            {
                _head = toAdd;
                _head.next = _head;
                _last = _head;
                _size++;
                return;
            }
            if (_size == 1 && index == 1)
            {
                _last = toAdd;
                _head.next = _last;
                _last.next = _head;
                _size++;
                return;
            }
            else
            {
                Element<T> _curr = _head;
                Element<T> _previous = null;
                for (int i = 0; i < index - 1; i++)
                {
                    _previous = _curr;
                    _curr = _curr.next;
                }
                _previous.next = toAdd;
                _previous = toAdd;
                _previous.next = _curr;
                _previous = _curr;
                _size++;
            }


        }
        public void Remove(int index)
        {
            Element<T> _toRemove = _head;
            Element<T> _previous = null;
            for (int i = 0; i < index; i++)
            {
                if (_toRemove.next == null)
                {
                    throw new IndexOutOfRangeException();
                }

                _previous = _toRemove;
                _toRemove = _toRemove.next;
            }
            //Index = 0 więc usuwamy głowę
            if (_previous == null)
            {
                _head = _toRemove.next;
                _size--;
                return;
            }

            //Usuwamy ostatni element
            if (_toRemove == _last)
            {
                _last = _previous;
                _last.next = null;
                _size--;
                return;
            }
            _previous.next = _toRemove.next;


            _toRemove.value = default;
            _size--;

        }
        public void RemoveLast()
        {
            Remove(_size - 1);
        }
        public void RemoveFirst()
        {
            Remove(0);
        }

        public T Get(int index)
        {
            Element<T> _toGet = _head;
            for (int i = 0; i < index; i++)
            {
                if (_toGet.next == null)
                {
                    throw new IndexOutOfRangeException();
                }
                _toGet = _toGet.next;
            }
            return _toGet.value;
        }
        public int Search(T data)
        {
            if (_size == 0)
            {
                throw new ArgumentNullException();
            }
            else
            {
                Element<T> _curr = _head;
                for (int i = 0; i < _size; i++)
                {
                    if (EqualityComparer<T>.Default.Equals(_curr.value, data))
                    {
                        return i;
                    }
                    _curr = _curr.next;
                }
            }
            throw new ArgumentNullException();
        }

        public void ShowAll()
        {
            if (_size == 0)
            {
                Console.WriteLine("Empty");
            }
            else
            {
                Element<T> _toShow = _head;
                for (int i = 0; i < _size; i++)
                {
                    Console.WriteLine(_toShow.value);
                    _toShow = _toShow.next;
                }
            }
        }
        public int Count()
        {
            return _size;
        }
    }
}
