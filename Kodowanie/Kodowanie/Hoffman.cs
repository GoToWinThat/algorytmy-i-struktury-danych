﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodowanie
{
    public class Hoffman
    {
        private List<string> sentence { get; set; }
        private List<KeyVal<string, double>> alphabet = new List<KeyVal<string, double>>();
        private List<KeyVal<string, double>> tree = new List<KeyVal<string, double>>();

        public Hoffman(string sent)
        {
            sentence = sent.Select(c => c.ToString()).ToList();
            GetAlphabet();
        }
        public void GetAlphabet()
        {
            foreach (var s in sentence)
            {
                if (!alphabet.Any(a => a.Attr == s))
                {
                    alphabet.Add(new KeyVal<string, double>
                    {
                        Attr = s,
                        Value = 1
                    });
                }
                else
                {
                    alphabet[(alphabet.FindIndex(a => a.Attr == s))].Value += 1;
                }
            }
            alphabet.OrderBy(a => a.Value);
        }
        public void CodeSentence()
        {
            CreateNewAlphabet();
            ShowCodedSentence();
        }
        private void CreateNewAlphabet()
        {
            //Utworzenie drzewa - ktory jest na poczatku kopiom alfabetu, ma litery wraz z ich wystąpieniami
            for (int i = 0; i < alphabet.Count(); i++)
            {
                KeyVal<string, double> temp = new KeyVal<string, double>(alphabet[i]);
                temp.Code.Add(temp.Attr);
                tree.Add(temp);
            }

            while (tree.Count() > 1)
            {
                //Nowy element
                KeyVal<string, double> toAdd = new KeyVal<string, double>();
                toAdd.Attr = "el" + tree.Count().ToString();
                toAdd.Value = 0;

                //Bierzemy elemnt z najnizsza wartoscia wystapien
                KeyVal<string, double> first = new KeyVal<string, double>(tree.OrderBy(a => a.Value).First());
                //usuwamy go
                tree = tree.Where(a => a.Attr != first.Attr).ToList();
                //do dodania wstawiamy usunieta litere
                toAdd.Value += first.Value;
                //Dajemu mu kod 0 - jako pierwsza najmniejsza
                foreach (var f in first.Code)
                {
                    alphabet[alphabet.FindIndex(a => a.Attr == f)].Code.Add("0");
                    toAdd.Code.Add(f);
                }
                //Drugie slowo wiec wszystko to samo
                KeyVal<string, double> second = new KeyVal<string, double>(tree.OrderBy(a => a.Value).First());
                tree = tree.Where(a => a.Attr != second.Attr).ToList();
                toAdd.Value += second.Value;
                //DAJEMY MU KOD 1 jako druga najmniejsza
                foreach (var s in second.Code)
                {
                    alphabet[alphabet.FindIndex(a => a.Attr == s)].Code.Add("1");
                    toAdd.Code.Add(s);
                }
                //Dodajemy polaczone litery/wyrazy spowrotem do drzewa
                tree.Add(toAdd);
            }
        }
        private void ShowCodedSentence()
        {
            foreach (var s in sentence)
            {
                int index = alphabet.FindIndex(a => a.Attr == s);
                for (int i = alphabet[index].Code.Count() - 1; i >= 0; i--)
                {
                    Console.Write(alphabet[index].Code[i]);
                }
                Console.Write("  ");
            }
        }
        /*public void DecodeSentence(string code)
        {
            Dictionary<string, string> letters = InitialLetterCodes();
            string word = "";
            string letterToCheck = "";
            for (int i = 0; i < code.Length; i++)
            {
                letterToCheck += code[i];
                if (!letters.ContainsKey(letterToCheck))
                {
                    if (!letters.ContainsKey(letterToCheck.Substring(0, letterToCheck.Length - 1)))
                    {
                        continue;
                    }
                    else
                    {
                        word += letters[letterToCheck.Substring(0, letterToCheck.Length - 1)];
                        letterToCheck = letterToCheck.Remove(0, letterToCheck.Length - 1);
                    }
                }
                if (i == code.Length - 1)
                {
                    word += letters[letterToCheck];
                }
            }
            Console.WriteLine(word);
        }*/
        /* private Dictionary<string, string> InitialLetterCodes()
         {
             Dictionary<string, string> letters = new Dictionary<string, string>();
             foreach (var a in alphabet)
             {
                 string letter = "";
                 for (int i = a.Code.Count() - 1; i >= 0; i--)
                 {
                     letter += Convert.ToString(a.Code[i]);
                 }
                 letters.Add(letter, a.Attr);
             }
             return letters;
         }*/

        private class KeyVal<Key, Val>
        {
            public Key Attr { get; set; }
            public Val Value { get; set; }

            public List<string> Code = new List<string>();

            public KeyVal() { }

            public KeyVal(Key key, Val val)
            {
                this.Attr = key;
                this.Value = val;
            }
            public KeyVal(KeyVal<Key,Val> copy)
            {
                Attr = copy.Attr;
                Value = copy.Value;
                Code = new List<string>(copy.Code);
            }
        }
    }
}
