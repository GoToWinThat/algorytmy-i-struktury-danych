﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodowanie
{
    class ShannonFano
    {
        private List<string> sentence { get; set; }
        private List<KeyVal<string, double>> alphabet = new List<KeyVal<string, double>>();
        Queue<KeyVal<int, int>> splitIndexer = new Queue<KeyVal<int, int>>();
        public ShannonFano(string sent)
        {
            sentence = sent.Select(c => c.ToString()).ToList();
            GetAlphabet();
        }
        public void CodeSentence()
        {
            CreateNewAlphabet();
            ShowCodedSentence();
        }
        public void DecodeSentence(string code)           
        {
            Dictionary<string, string> letters = InitialLetterCodes();
            string word = "";
            string letterToCheck = "";
            for(int i=0; i<code.Length;i++)
            {
                letterToCheck += code[i];
                if (!letters.ContainsKey(letterToCheck))
                {
                    if(!letters.ContainsKey(letterToCheck.Substring(0, letterToCheck.Length - 1)))
                    {
                        continue;
                    }
                    else
                    {
                        word += letters[letterToCheck.Substring(0, letterToCheck.Length - 1)];
                        letterToCheck = letterToCheck.Remove(0, letterToCheck.Length - 1);
                    }
                }
                if(i==code.Length-1)
                {
                    word += letters[letterToCheck];
                }
            }
            Console.WriteLine(word);
        }
        private Dictionary<string, string> InitialLetterCodes()
        {
            Dictionary<string, string> letters = new Dictionary<string, string>();
            foreach(var a in alphabet)
            {
                string letter = "";
                foreach(var c in a.Code)
                {
                    letter += Convert.ToString(c);
                }
                letters.Add( letter,a.Attr);
            }
            return letters;
        }
        public void GetAlphabet()
        {
            foreach (var s in sentence)
            {
                if (!alphabet.Any(a=>a.Attr==s))
                {
                    alphabet.Add(new KeyVal<string, double>
                    {
                        Attr = s,
                        Value = 1
                    });
                }
                else
                {
                    alphabet[(alphabet.FindIndex(a => a.Attr == s))].Value += 1;
                }
            }
            alphabet.OrderBy(a => a.Value);
        }
        private void CreateNewAlphabet()
        {
            splitIndexer.Enqueue(new KeyVal<int, int> { Attr = 0, Value = alphabet.Count() });
            while(splitIndexer.Count()!=0)
            {
                KeyVal<int, int> toSplit = splitIndexer.Dequeue();
                if(toSplit.Attr-(toSplit.Value-1)!=0)
                {
                    Split(toSplit.Attr, toSplit.Value);
                }
            }
        }
        private void Split(int start, int end)
        {
            List<double> diff = new List<double>();
            for(int i=start; i<end-1;i++)
            {
                double first = 0;
                double second = 0;
                for(int a=start;a<=i;a++)
                {
                    first += alphabet[a].Value;
                }
                for (int b = i+1; b < end; b++)
                {
                    second += alphabet[b].Value;
                }
                diff.Add(Math.Abs(first - second));
            }
            int splitIndex = diff.IndexOf(diff.Min());
            for(int i=0;i<=end-start-1;i++)
            {
                if(i<=splitIndex)
                {
                    alphabet[i + start].Code.Add("0");
                }
                else
                {
                    alphabet[i + start].Code.Add("1");
                }
            }
            splitIndexer.Enqueue(new KeyVal<int, int> { Attr = start, Value = start + splitIndex + 1 });
            splitIndexer.Enqueue(new KeyVal<int, int> { Attr = start + splitIndex + 1, Value = end });
        }
        private void ShowCodedSentence()
        {
            foreach(var s in sentence)
            {
                int index = alphabet.FindIndex(a => a.Attr == s);
                foreach(var n in alphabet[index].Code)
                {
                    Console.Write(n);
                }
                Console.Write("  ");
            }
        }
        private class KeyVal<Key, Val>
        {
            public Key Attr { get; set; }
            public Val Value { get; set; }
            public List<string> Code = new List<string>();
            public KeyVal() { }
            public KeyVal(Key key, Val val)
            {
                this.Attr = key;
                this.Value = val;
            }
        }
    }
}
