﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodowanie
{
    class Caesar
    {
        public string CodeSentence(string sent, int shift)
        {
            char[] buffer = sent.ToCharArray();
            for (int i = 0; i < buffer.Length; i++)
            {

                char letter = buffer[i];
                if (char.IsUpper(letter))
                {
                    letter =char.ToLower(letter);
                    buffer[i] = char.ToUpper(ConvertChar(letter, shift));
                }
                else
                {
                    buffer[i] =(ConvertChar(letter, shift));
                }
            }
            return new string(buffer);
        }
        private char ConvertChar(char letter, int shift)
        {
            letter = (char)(letter + shift);

            if (letter > 'z')
            {
                letter = (char)(letter - 26);
            }
            else if (letter < 'a')
            {
                letter = (char)(letter + 26);
            }
            return letter;
        }
        public string DecodeSentence(string sent, int shift)
        {
            return CodeSentence(sent, shift * -1);
        }
    }
}
