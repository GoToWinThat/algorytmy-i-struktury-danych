﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyszukiwanie_Wzorca
{
    class Program
    {
        static void Main(string[] args)
        {
            BoyerMoore BM = new BoyerMoore("WIKIPEDIA, WOLNA ENCYKLOPEDIA TO FAJNA CYKLOPEDIA O ROZNYCH CYKLACH", "CYKL");
            BM.SearchPattern();
            KMP kmp = new KMP("WIKIPEDIA, WOLNA ENCYKLOPEDIA TO FAJNA CYKLOPEDIA O ROZNYCH CYKLACH", "CYKL");
            kmp.SearchPattern();
            KarpRabin kr = new KarpRabin("WIKIPEDIA, WOLNA ENCYKLOPEDIA TO FAJNA CYKLOPEDIA O ROZNYCH CYKLACH", "CYKL");
            kr.SearchPattern();
            Console.ReadKey();
        }   
    }
}
