﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyszukiwanie_Wzorca
{
    public class BoyerMoore
    {
        private List<string> sentence { get; set; }
        private List<string> pattern { get; set; }
        private int patternLenght { get; set; }
        private int sentenceLenght { get; set; }
        private List<int> findedPatterns = new List<int>();
        public BoyerMoore(string sent, string pat)
        {
            sentence = sent.Select(c => c.ToString()).ToList();
            pattern = pat.Select(c => c.ToString()).ToList();
            patternLenght = pattern.Count();
            sentenceLenght = sentence.Count();
        }
        public void SearchPattern()
        {
            int flag = patternLenght-1;
            while(flag<sentenceLenght)
            {
                if(pattern.Contains(sentence[flag]))
                {
                    List<int> index = FindLetterIndex(sentence[flag]);
                    for(int i=0;i<index.Count();i++)
                    {
                        List<string> word = ExtractWord(index[i], flag);
                        if (CompareWord(word))
                        {
                            findedPatterns.Add(flag - index[i]);
                        }
                    }
                }
                flag += patternLenght;
            }
            ShowSolution();
        }
        private List<int> FindLetterIndex(string letter)
        {
            List<int> index = new List<int>();
            for(int i=0; i<patternLenght; i++)
            {
                if(pattern[i]==letter)
                {
                    index.Add(i);
                }
            }
            return index;
        }
        private List<string> ExtractWord(int index,int flag)
        {
            if (flag - index+patternLenght<=sentenceLenght)
            {
                List<string> word = sentence.GetRange(flag - index, patternLenght);
                return word;
            }
            return new List<string> {""};
        }
        private bool CompareWord(List<string> word)
        {
            return pattern.SequenceEqual(word);
        }
        private void ShowSolution()
        {
            foreach(var f in findedPatterns)
            {
                Console.WriteLine(f);
            }
        }
    }
}
