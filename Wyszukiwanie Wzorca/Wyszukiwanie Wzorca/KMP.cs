﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyszukiwanie_Wzorca
{
    public class KMP
    {
        private List<string> sentence { get; set; }
        private List<string> pattern { get; set; }
        private int patterLenght { get; set; }
        private int sentenceLenght { get; set; }
        private List<int> prefixFunction;
        private List<int> findedPatterns = new List<int>();
        public KMP(string sent, string pat)
        {
            sentence = sent.Select(c => c.ToString()).ToList();
            pattern = pat.Select(c => c.ToString()).ToList();
            patterLenght = pattern.Count();
            sentenceLenght = sentence.Count();
            prefixFunction = new List<int>(new int[patterLenght]);
        }

        public void SearchPattern()
        {
            KMP_MATCHER();
            ShowSolution();
        }
        private void KMP_MATCHER()
        {
            ComputePrefixFunction();
            int q = -1;
            for(int i=0;i<sentenceLenght;i++)
            {
                while (q > 0 && pattern[q+1] != sentence[i])
                {
                    q = prefixFunction[q];
                }
                if (pattern[q+1] == sentence[i])
                {
                    q += 1;
                }
                if(q==patterLenght-1)
                {
                    findedPatterns.Add(i - patterLenght+1);
                    q = prefixFunction[q];
                }
            }
        }
        private void ComputePrefixFunction()
        {
            prefixFunction[0] = 0;
            int k = -1;

            for (int q=1; q<patterLenght;q++)
            {
                while(k>0 && pattern[k+1]!=pattern[q])
                {
                    k = prefixFunction[k];
                }
                if(pattern[k+1]==pattern[q])
                {
                    k += 1;
                }

                prefixFunction[q] = k;
            }
        }
        private void ShowSolution()
        {
            foreach (var f in findedPatterns)
            {
                Console.WriteLine(f);
            }
        }
    }
}
