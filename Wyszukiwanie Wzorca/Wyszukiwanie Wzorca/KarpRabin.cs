﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyszukiwanie_Wzorca
{
    public class KarpRabin
    {
        private int d {get;set;}
        private List<int> sentence = new List<int>();
        private List<int> pattern = new List<int>();
        private int patterLenght { get; set; }
        private int sentenceLenght { get; set; }
        private List<int> findedPatterns = new List<int>();
        public KarpRabin(string sent, string pat)
        {
            d = GetAlphabet(sent.Select(c => c.ToString()).ToList(), pat.Select(c => c.ToString()).ToList());
            foreach (char c in sent)
            {
                sentence.Add(System.Convert.ToInt32(c));
            }
            foreach (char c in pat)
            {
                pattern.Add(System.Convert.ToInt32(c));
            }
            patterLenght = pattern.Count();
            sentenceLenght = sentence.Count();
        }
        public void SearchPattern()
        {
            double q = 10729;
            double h = Math.Pow(d, patterLenght - 1);
            double p = 0;
            double t0 = 0;
            List<double> moveTab = new List<double>();
            for(int i=0; i<patterLenght;i++)
            {
                p = (d * p + pattern[i]) % q;
                t0 = (d * t0 + sentence[i]) % q;
            }
            moveTab.Add(t0);
            for(int s=0; s<=sentenceLenght-patterLenght;s++)
            {
                if(p==moveTab[s])
                {
                    if(CompareWord(ExtractWord(s)))
                    {
                        findedPatterns.Add(s);
                    }
                }
                if(s< sentenceLenght - patterLenght)
                {
                    double toAdd = (d * (moveTab[s] - sentence[s] * h) + (sentence[s + patterLenght])) % q;
                    if(toAdd<0)
                    {
                        toAdd = toAdd + q;
                    }
                    moveTab.Add(toAdd);
                }
            }
            ShowSolution();
        }
        public int GetAlphabet(List<string> sent, List<string> patt)
        {
            List<string> alf = new List<string>();
            foreach(var s in sent)
            {
                if(!alf.Contains(s))
                {
                    alf.Add(s);
                }
            }
            foreach (var p in patt)
            {
                if (!alf.Contains(p))
                {
                    alf.Add(p);
                }
            }
            return alf.Count();
        }
        private List<int> ExtractWord(int flag)
        {
            if (flag + patterLenght <= sentenceLenght)
            {
                List<int> word = sentence.GetRange(flag, patterLenght);
                return word;
            }
            return new List<int> { 0 };
        }
        private bool CompareWord(List<int> word)
        {
            return pattern.SequenceEqual(word);
        }
        private void ShowSolution()
        {
            foreach(var f in findedPatterns)
            {
                Console.WriteLine(f);
            }
        }
    }
}
