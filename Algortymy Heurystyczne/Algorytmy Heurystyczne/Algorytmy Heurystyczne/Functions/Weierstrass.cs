﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne.Functions
{
    public class Weierstrass 
    {

        public double Func(double[] input)
        {
            double result = 0;
            for (int i = 0; i < input.Length; i++)
            {
                result += Math.Pow(Math.Abs(input[i] + 0.5), 2.0);
            }
            return result;
        }
    }
}
