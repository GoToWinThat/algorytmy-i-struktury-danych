﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne.Functions
{
    public class StyblinskiTang 
    {
        public double Func(double[] input)
        {
            double result = 0;
            for (int i = 0; i < input.Length; i++)
            {
                result += Math.Pow(input[i], 4.0) - (16.0 * input[i] * input[i]) + (5.0 * input[i]);
            }
            return 0.5 * result;
        }
    }
}

