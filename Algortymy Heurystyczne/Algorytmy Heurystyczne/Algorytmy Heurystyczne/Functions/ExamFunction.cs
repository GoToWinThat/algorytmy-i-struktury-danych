﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne
{
    public class ExamFunction
    {
        public double Func(double[] input)
        {
            return (-Math.Floor(input[0] * input[1]) + Math.Pow(input[0], 2) + Math.Pow(input[1], 2));
        }
    }
}
