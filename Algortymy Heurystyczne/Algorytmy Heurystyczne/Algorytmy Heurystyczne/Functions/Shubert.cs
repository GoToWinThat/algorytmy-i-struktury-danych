﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne
{
    public class Shubert 
    {
        public double Func(double[] input)
        {
            double result = 1;
            for (int i = 0; i < input.Length; i++)
            {
                double temp = 0;
                for (int j = 1; j < 6; j++)
                {
                    temp += (double)j * Math.Cos(((j + 1) * input[i]) + (double)j);
                }
                result *= temp;
            }
            return result;
        }
    }
}
