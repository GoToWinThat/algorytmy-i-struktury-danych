﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne
{
    public class Rosenbrock
    {
        public double Func(double[] input)
        {
            double result = 0;
            for (int i = 0; i < input.Length - 1; i++)
            {
                result += 100.0 *  Math.Pow(input[i + 1] - (input[i] * input[i]), 2.0) +      Math.Pow(input[i] - 1.0, 2.0);
            }
            return result;
        }
    }
}
