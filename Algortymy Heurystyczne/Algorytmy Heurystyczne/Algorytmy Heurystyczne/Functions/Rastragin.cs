﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne.Functions
{
    public class Rastragin 
    {
        public double Func(double[] input)
        {
            double result = 10.0 * input.Length;

            for (int i = 0; i < input.Length; i++)
            {
                result += (input[i] * input[i]) - 10.0 * Math.Cos(2.0 * Math.PI * input[i]);
            }
            return result;
        }
    }
}
