﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne.Functions
{
    public class Sphere 
    {
        public double Func(double[] input)
        {
            double result = 0;
            for (int i = 0; i < input.Length; i++)
            {
                result += input[i] * input[i];
            }
            return result;
        }
    }
}
