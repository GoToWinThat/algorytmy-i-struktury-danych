﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne
{
    public class Genetic
    {
        private static Random rand = new Random();
       
        public double[] Find(Func<double[],double> function, double[] searchSection, int generationCount, int populationCount, int dimension, double mutationChance, double mutationPower)
        {
            List<DNA> population = new List<DNA>();
            for (int i = 0; i < populationCount; i++)
            {
                double[] code = CreateCode(dimension, searchSection[0], searchSection[1]);
                population.Add(new DNA(code));
            }

            for (int i = 0; i < generationCount; i++)
            {
                foreach(var p in population)
                {
                    p.CalculateWeight(function);
                }
                population = population.OrderBy(x => x.weight).ToList();
                population.RemoveRange((int)(population.Count * 0.5), population.Count - (int)(population.Count * 0.5));

                List<DNA> combined = new List<DNA>();
                while (combined.Count + population.Count < populationCount)
                {
                    DNA first = population[rand.Next(0, population.Count)];
                    DNA second = population[rand.Next(0, population.Count)];
                    combined.Add(CreateChild(first, second,dimension));
                }
                population.AddRange(combined);
                combined.Clear();
                foreach (DNA dna in population)
                {
                    double mutation = rand.NextDouble();
                    if (mutation < mutationChance)
                    {
                        Mutate(dna, searchSection, mutationPower); 
                    }
                }
            }

            foreach (var dna in population)
            {
                dna.CalculateWeight(function);
            }
            population = population.OrderBy(x => x.weight).ToList();
            return population[0].code;
        }

        private void Mutate(DNA dna, double[] searchSection, double mutationPower)
        {
            for (int i = 0; i < dna.code.Length; i++)
            {
                dna.code[i] += dna.code[i] * mutationPower * (rand.NextDouble() - 0.5);
                if(dna.code[i]< searchSection[0])
                {
                    dna.code[i] = searchSection[0];
                }
                else if (dna.code[i]>  searchSection[1])
                {
                    dna.code[i] = searchSection[1];
                }
            }
        }
        private DNA CreateChild(DNA first, DNA second,int dimension)
        {
            double[] childGenetics = new double[dimension];
            for (int i = 0; i < dimension; i++)
            {
                int firstORsecond = rand.Next(0, 2);
                if(firstORsecond==0)
                {
                    childGenetics[i] = first.code[i];
                }
                else
                {
                    childGenetics[i] = second.code[i];
                }
            }

            return new DNA(childGenetics);
        }
        private double[] CreateCode(int counter, double start,double end)
        {
            double[] code = new double[counter];
            for (int i = 0; i < counter; i++)
            {
                code[i] = start + (rand.NextDouble() *(end-start));
            }
            return code;
        }
        private class DNA
        {
            public double[] code;
            public double weight;

            public DNA(double[] code)
            {
                this.code = code;
            }
            public void CalculateWeight(Func<double[], double> function)
            {
                weight = function(code);
            }
        }
    }
}
