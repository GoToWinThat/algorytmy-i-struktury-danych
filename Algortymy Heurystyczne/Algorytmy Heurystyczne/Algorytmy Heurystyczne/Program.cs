﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorytmy_Heurystyczne.Functions;
namespace Algorytmy_Heurystyczne
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Genetic tests*/
            /*
            Genetic Genetic = new Genetic();
           
            Weierstrass Weierstrass = new Weierstrass();
            SumSquares SumSquares = new SumSquares();
       
            double[] resultWeierstrass = Genetic.Find(Weierstrass.Func, new double[] { -30.0, 30.0 },15000,1000,10,0.2,0.3);
            double solutionWeierstrass = Weierstrass.Func(resultWeierstrass);

            double[] resultSumSquares = Genetic.Find(SumSquares.Func, new double[] { -10.0, 10.0 }, 15000, 1000, 10, 0.3, 0.3);
            double solutionSumSquares = SumSquares.Func(resultSumSquares);
            */

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /*Annealing tests*/ //Algortym według opisu z atykułu wikipedii - działa dobrze, lecz małą dokładnością, dwóch do trzech miejsc po przecinku
                         
            Annealing annealing = new Annealing();
            
            Weierstrass Weierstrass = new Weierstrass();
            SumSquares SumSquares = new SumSquares();
            HyperEllipsoid HyperEllipsoid = new HyperEllipsoid();

            double[] resultWeierstrass = annealing.Find(Weierstrass.Func, new double[] { -30.0, 30.0 },10000000, 10, 3000, 0.999);
            double solutionWeierstrass = Weierstrass.Func(resultWeierstrass);

           /* double[] resultSumSquares = annealing.Find(SumSquares.Func, new double[] { -10.0, 10.0 }, 50000, 10, 1200, 0.999);
            double solutionSumSquares = SumSquares.Func(resultSumSquares);

            double[] resultHyperEllipsoid = annealing.Find(HyperEllipsoid.Func, new double[] { -10.0, 10.0 }, 75000, 10, 1500, 0.999);
            double solutionSHyperEllipsoid = HyperEllipsoid.Func(resultSumSquares);*/
                               
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /*Differential tests*/
            /*   
            Differential differential = new Differential();

            Weierstrass Weierstrass = new Weierstrass();
            SumSquares SumSquares = new SumSquares();
            HyperEllipsoid HyperEllipsoid = new HyperEllipsoid();

            double[] resultWeierstrass = differential.Find(Weierstrass.Func, new double[] { -30.0, 30.0 }, 5000, 1000, 10, 0.5,1);
            double solutionWeierstrass = Weierstrass.Func(resultWeierstrass);

            double[] resultSumSquares = differential.Find(SumSquares.Func, new double[] { -10.0, 10.0 }, 5000, 1000, 10, 0.5, 1);
            double solutionSumSquares = SumSquares.Func(resultSumSquares);

            double[] resultHyperEllipsoid = differential.Find(HyperEllipsoid.Func, new double[] { -100.0, 100.0 }, 5000, 1000, 10, 0.5, 1);
            double solutionSHyperEllipsoid = HyperEllipsoid.Func(resultSumSquares);
            */

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///
            /*Bees tests*/
            /*
            Bees Bees = new Bees();
            Weierstrass Weierstrass = new Weierstrass();
            HyperEllipsoid HyperEllipsoid = new HyperEllipsoid();
            SumSquares SumSquares = new SumSquares();

            double[] resultWeierstrass = Bees.Find(Weierstrass.Func, new double[] { -30.0, 30.0 },10,10000,1000);
            double solutionWeierstrass = Weierstrass.Func(resultWeierstrass);

            double[] resultSumSquares = Bees.Find(SumSquares.Func, new double[] { -10.0, 10.0 }, 10, 10000, 1000);
            double solutionSumSquares = SumSquares.Func(resultSumSquares);

            double[] resultHyperEllipsoid = Bees.Find(HyperEllipsoid.Func, new double[] { -100.0, 100.0 }, 10, 10000, 1000);
            double solutionSHyperEllipsoid = HyperEllipsoid.Func(resultSumSquares);
            */
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /*Swarm tests*/
            /*
            Swarm Swarm = new Swarm();
            Weierstrass Weierstrass = new Weierstrass();
            HyperEllipsoid HyperEllipsoid = new HyperEllipsoid();
            SumSquares SumSquares = new SumSquares();

            double[] resultWeierstrass = Swarm.Find(Weierstrass.Func, new double[] { -30.0, 30.0 }, 10, 10000, 100, 0.5, 0.75, 2);
            double solutionWeierstrass = Weierstrass.Func(resultWeierstrass);

            double[] resultSumSquares = Swarm.Find(SumSquares.Func, new double[] { -10.0, 10.0 },10,10000, 100, 0.5,0.75,2);
            double solutionSumSquares = SumSquares.Func(resultSumSquares);

            double[] resultHyperEllipsoid = Swarm.Find(HyperEllipsoid.Func, new double[] { -100.0, 100.0 }, 10, 10000, 100, 0.5, 0.75, 2);
            double solutionSHyperEllipsoid = HyperEllipsoid.Func(resultSumSquares);
            */
        }
    }
}
