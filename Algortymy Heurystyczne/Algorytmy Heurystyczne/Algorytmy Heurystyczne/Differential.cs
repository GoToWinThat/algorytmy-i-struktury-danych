﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne
{
    public class Differential
    {
        private static Random rand = new Random();
        public double[] Find(Func<double[], double> func, double[] searchSection, int iterationsCounter, int populationCounter,int dimension, double crossoverProbability, double differentialWeight)
        {
            List<Agent> population = new List<Agent>();
            for (int i = 0; i < populationCounter; i++)
            {
                population.Add(CreateAgent(dimension));
            }

            //Główna pętla
            for (int i = 0; i < iterationsCounter; i++)
            {
                //Pętla każdego agenta
                for (int j = 0; j < population.Count; j++)
                {
                    //Losowanie trzech agentów
                    List<Agent> selectedAgents = SelectAgents(population, j);
                    double[] newAgent = new double[dimension];

                    for (int k = 0; k < dimension; k++)
                    {
                        double r = rand.NextDouble();

                        //Jezeli warunek prawdopodobienstwa jest spelniony
                        //Zmienna danego wymiaru zmieniana jest ze wzoru:
                        //Agent A + waga * roznica_agentów(B- C) - w ramach indeksu k od 0 do n wymiarów
                        if (r < crossoverProbability)
                        {
                            newAgent[k] = selectedAgents[0].solution[k] + differentialWeight * (selectedAgents[1].solution[k] - selectedAgents[2].solution[k]);
                        }
                        //Jeżeli nie to kopiujemy wartosc z agenta z populacji
                        else
                        {
                            newAgent[k] = population[j].solution[k];
                        }
                        //Sprawdzanie warunków brzegowych poszukiwań
                        if (newAgent[k] < searchSection[0])
                        {
                            newAgent[k] = searchSection[0];
                        }                      
                        if (newAgent[k] > searchSection[1])
                        {
                            newAgent[k] = searchSection[1];
                        }
                    }
                    if (func(newAgent) <= func(population[j].solution) )
                    {
                        population[j].solution = newAgent;
                    }                       
                }
            }
            population.OrderBy(x => func(x.solution));
            return population[0].solution;
        }

        private Agent CreateAgent(int counter)
        {
            double[] solution = new double[counter];
            for (int i = 0; i < counter; i++)
            {
                solution[i] = rand.NextDouble();
            }
            return new Agent(solution);
        }
        private List<Agent> SelectAgents(List<Agent> population, int current)
        {
            List<Agent> result = new List<Agent>();
            Stack<int> agenstIndex = new Stack<int>();
            while(agenstIndex.Count<3)
            {
                int index = rand.Next(0, population.Count);
                if(!agenstIndex.Contains(index))
                {
                    agenstIndex.Push(index);
                }
               
            }
            result.Add(new Agent(population[agenstIndex.Pop()].solution));
            result.Add(new Agent(population[agenstIndex.Pop()].solution));
            result.Add(new Agent(population[agenstIndex.Pop()].solution));
            return result;
        }
        private class Agent
        {
            public double[] solution;
            public Agent(double[] solution)
            {
                this.solution = solution;
            }
        }
    }
}
