﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne
{
    public class Annealing
    {
        static Random rand = new Random();

        public double[] Find(Func<double[], double> func, double[] searchSpace, int iterationsCounter, int dimension, double temperature,double colding)
        {

            double[] bestSolution = CreateCode(dimension, searchSpace[0], searchSpace[1]);

            for (int i = 0; i < iterationsCounter; i++)
            {

                double[] newSolution = new double[dimension];
                bestSolution.CopyTo(newSolution, 0);
                for (int j = 0; j < dimension; j++)
                {
                    //newSolution[j] += NewValueWeight() * temperature;
                    newSolution[j] += 0.001 * CustomRandom(searchSpace);
                }
                if (func(newSolution) < func(bestSolution))
                {
                    bestSolution = newSolution;
                }
                else
                {
                    if (CalculateChange(func(newSolution), func(bestSolution), temperature) >= rand.NextDouble())
                    {
                        bestSolution = newSolution;
                    }
                }
                temperature *= colding;
            }
            return bestSolution;
        }
        static double CalculateChange(double newT, double oldT, double currTemp)
        {
            return Math.Exp(-(newT - oldT) / currTemp);
        }
        private double[] CreateCode(int counter, double start, double end)
        {
            double[] code = new double[counter];
            for (int i = 0; i < counter; i++)
            {
                code[i] = start + (rand.NextDouble() * (end - start));
            }
            return code;
        }
        public double NewValueWeight()
        {
            return 2 * rand.NextDouble() - 1;
        }
        public double CustomRandom(double[] searchSpace)
        {
            return rand.NextDouble() * (searchSpace[1]- searchSpace[0]) + searchSpace[0];
        }
    }
}
