﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne
{
    public class Swarm
    {
        private static Random rand = new Random();
        public double[] Find(Func<double[], double> function, double[] serachSection, int dimension, int iterationsCounter, int populationCounter, double velocityWeight, double bestSolutionWeight, double bestSwarmSolutionWeight)
        {
            //Inicjalizacja oraz ustawienie najlepszego rozwiazania
            List<Particle> population = new List<Particle>();
            double[] bestSwarmPosition = CreateCode(dimension, serachSection[0], serachSection[1]);

            for (int i = 0; i < populationCounter; i++)
            {
                population.Add(new Particle(CreateCode(dimension, serachSection[0], serachSection[1]),
                CreateCode(dimension, -1.0, 1.0)));

                if (function(population.Last().benchmark) < function(bestSwarmPosition))
                {
                    bestSwarmPosition = population.Last().benchmark;
                }
            }

            for (int i = 0; i < iterationsCounter; i++)
            {
                //Przeszukujemy wszystkie rekordy
                foreach (Particle part in population)
                {
                    for (int j = 0; j < dimension; j++)
                    {
                        //Losujemy współczynniki
                        double randomPositionFactor = rand.NextDouble();
                        double randomSwarmFactor = rand.NextDouble();

                        //Nowa predkosc to suma trzech skladnikow
                        //Pierwszy: iloczyn stalej_predkosci i predkosci
                        //Drugi: Iloczyn stalej_pozycji, wylosowanej_stalej_pozycji oraz roznica (pozycji_najlepszej i aktualnej)
                        //Trzeci: Iloczyn stałej_roju, wylosowanej_stalej_roju oraz roznicy (pozycji_najlepszej_roju i aktualnej_pozycji_czasteczki)
                        part.velocity[j] = (part.velocity[j] * velocityWeight)
                            + bestSolutionWeight * randomPositionFactor * (part.benchmark[j] - part.position[j])
                            + bestSwarmSolutionWeight * randomSwarmFactor * (bestSwarmPosition[j] - part.position[j]);
                    }
                    //Zmiana połozenia o wektor prędkości
                    for (int j = 0; j < dimension; j++)
                    {
                        part.position[j] += part.velocity[j];
                    }
                    //Aktualizacja czasteczki
                    if (function(part.position) < function(part.benchmark))
                    {
                        part.benchmark = part.position;
                    }
                    //Aktualizacja całego roju
                    if (function(part.position) < function(bestSwarmPosition))
                    {
                        bestSwarmPosition = part.position;
                    }
                }
            }
            return bestSwarmPosition;
        }
        private double[] CreateCode(int counter, double start, double end)
        {
            double[] code = new double[counter];
            for (int i = 0; i < counter; i++)
            {
                code[i] = start + (rand.NextDouble() * (end - start));
            }
            return code;
        }
        private class Particle
        {
            public double[] velocity;
            public double[] position;
            public double[] benchmark;

            public Particle(double[] position, double[] velocity)
            {
                this.position = position;
                this.velocity = velocity;
                benchmark = position;
            }
        }
    }
}
