﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy_Heurystyczne
{
    public class Bees
    {
        private static Random rand = new Random();

        public double[] Find(Func<double[], double> function, double[] searchSection, int dimension, int iterationsCounter,int populationCounter)
        {       
            //Tworzymy populacje z losowych liczb
            List<Bee> population = new List<Bee>();
            for (int i = 0; i < populationCounter; i++)
            {
                population.Add(new Bee(CreateCode(dimension, searchSection[0], searchSection[1])));
            }
            //Iterator generacji
            for (int i = 0; i < iterationsCounter; i++)
            {
                //Ustawienie wartości rozwiązań
                foreach (var p in population)
                {
                    p.CalculateWeight(function);
                }
                //Ruch pszczół
                for (int j = 0; j < population.Count; j++)
                {
                    //Zapisujemy pozycje
                    double[] newSolution = new double[dimension];
                    population[j].position.CopyTo(newSolution, 0);

                    //Bierzemy losowy wymiar w pozycji
                    int newDimension = rand.Next(0, dimension);

                    //Losujemy nową pszczołę - ale nie tą samą
                    int otherBeeIndex = rand.Next(0, population.Count);
                    if(otherBeeIndex==j)
                    {
                        if(j== population.Count)
                        {
                            otherBeeIndex--;
                        }
                        if (j == 0)
                        {
                            otherBeeIndex++;
                        }
                    }
                    //Nowa pozycja według wzoru algorytmu
                    newSolution[newDimension] = 
                        population[j].position[newDimension] 
                        + RandomNormal(-1, 1, 1) 
                        * (population[j].position[newDimension] 
                        - population[otherBeeIndex].position[newDimension]);

                    //Porównanie nowego rozwiązania
                    double possibleFitness = function(newSolution);
                    
                    //Wybranie lepszej pozycji
                    if (possibleFitness < population[j].weight)
                    {
                        population[j].position = newSolution;
                        population[j].weight = possibleFitness;
                    }
                }
                //Usuwanie gorszych poszukiwaczy wśród pszczół
                population.OrderBy(x => x.weight).ToList();
                population.RemoveRange((int)(population.Count * 0.4), population.Count - (int)(population.Count * 0.4));

                //Tworzenie nowych
                List<Bee> combined = new List<Bee>();
                while (combined.Count + population.Count < populationCounter)
                {
                    combined.Add(new Bee(CreateCode(dimension, searchSection[0], searchSection[1])));
                }
                population.AddRange(combined);
                combined.Clear();
            }
            population.OrderBy(x => x.weight).ToList();
            return population[0].position;
        }
        private class Bee
        {
            public double[] position;
            public double weight = double.MaxValue;

            public Bee(double[] position)
            {
                this.position = position;
            }
            public void CalculateWeight(Func<double[], double> function)
            {
                weight = function(position);
            }
        }
        private double[] CreateCode(int counter, double start, double end)
        {
            double[] code = new double[counter];
            for (int i = 0; i < counter; i++)
            {
                code[i] = start + (rand.NextDouble() * (end - start));
            }
            return code;
        }
        public double RandomNormal( double min, double max, int tightness)
        {
            double total = 0.0;
            for (int i = 1; i <= tightness; i++)
            {
                total += rand.NextDouble();
            }
            double test=((total / tightness) *(max - min)) +min;
            return test;
        }
    }
}

