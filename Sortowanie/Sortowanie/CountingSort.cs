﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortowanie
{
    class CountingSort
    {
        public int[] Sort(int[] data)
        {
            int[] result = new int[data.Length];
            //Okreslenie zakresu danych
            int maxVal = data[0];
            for (int i = 1; i < data.Length; i++)
            {
                if (data[i] > maxVal)
                {
                    maxVal = data[i];
                } 
            }
            //Przygotowanie Histogramu
            int lenght = maxVal > data.Length ? maxVal : data.Length;
            int[] count = new int[lenght];
            for (int i = 0; i < data.Length; i++)
            {
                count[i] = 0;
            }
            //Dodanie danych histogramu
            for (int i = 0; i < data.Length; i++)
            {
                count[data[i]-1]++;
            }
            //Korygowanie wartosci zwiazanej z indeksami od zera
            for (int i = 1; i <= lenght-1; ++i)
            {
                count[i] += count[i - 1];
            } 
            //Utworzenie posortowanego dzialu gdzie danych element wpisany jest tyle razy ile zliczylismy w histogramie
            //Pozycja jest okreslana za zasadzie "ile wartosci jest mniejszych od danej wartosci"
            for (int i = data.Length - 1; i >= 0; i--)
            {
                result[count[data[i]-1] - 1] = data[i];
                count[data[i]-1]--;
            }
            return result;
        }
    }
}
