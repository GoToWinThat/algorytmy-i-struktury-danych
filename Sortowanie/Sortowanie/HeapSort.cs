﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortowanie
{
    class HeapSort
    {
        //Przesuwanie elementow  w dol kopca
        private void Down(int[] data, int start, int end)
        {
            int begin = start;
            while (begin * 2 + 1 < end)
            {
                int under = begin * 2 + 1;
                int temp = begin;
                if (data[temp] < data[under])
                {
                    temp = under;
                }
                if (under + 1 <= end && data[temp] < data[under + 1])
                {
                    temp = under + 1;
                }
                if (temp != begin)
                {
                    int tmp = data[temp];
                    data[temp] = data[begin];
                    data[begin] = tmp;
                    begin = temp;
                }
                else
                {
                    return;
                }
            }
        }
        //Buduwanie kopca
        private void Heapify(int[] data, int count)
        {
            //Bierzemy polowe zbioru
            int start = count / 2 - 1;
            //Dopoki mamy elementy na kopcu
            while (start >= 0)
            {
                //Przesuwamy elementy w dol kopca
                Down(data, start, count - 1);
                start--;
            }
        }
        //Sortowanie
        public int[] HeapSortS(int[] data, int count)
        {
            //Tworzymy główny kopiec
            Heapify(data, count);
            int end = count - 1;
            while (end > 0)
            {
                //Podmiana zmiennej
                int temp = data[end];
                data[end] = data[0];
                data[0] = temp;
                //Przesuwanie reszty elementó w dół
                Down(data, 0, end - 1);
                end--;
            }
            return data;
        }
    }
}
