﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortowanie
{
    public class BubbleSort
    {
        public T[] Sort<T>(T[] data) where T:IComparable
        {
            for (int i = 0; i <= data.Length-2; i++)
            {
                for (int j = 0; j <= data.Length - 2; j++)
                {
                    if (data[j].CompareTo(data[j + 1])>0)
                    {
                        T temp = data[j + 1];
                        data[j + 1] = data[i];
                        data[j] = temp;
                    }
                }
            }
            return data;
        }
    }
}
