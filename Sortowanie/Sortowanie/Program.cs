﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortowanie
{
    class Program
    {
        static void Main(string[] args)
        {
            List<long> times = new List<long>();
            int[] small = GenerateRandomNumbers(1000);
            int[] medium = GenerateRandomNumbers(100000);
            int[] large = GenerateRandomNumbers(500000);
            CountingSort cs = new CountingSort();
            HeapSort hs = new HeapSort();
            BubbleSort bs = new BubbleSort();
            MergeSort ms = new MergeSort();
            QuickSort qs = new QuickSort();
            ShellSort ss = new ShellSort();


            /*var watch = System.Diagnostics.Stopwatch.StartNew();
            int[] sCounting = cs.Sort(small);
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            times.Add(elapsedMs);*/

            /*var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] mCounting = cs.Sort(medium);
             var elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);*/

            /*var watch = System.Diagnostics.Stopwatch.StartNew();
            int[] lCounting = cs.Sort(large);
            var elapsedMs = watch.ElapsedMilliseconds;
            times.Add(elapsedMs);*/

            /* var watch = System.Diagnostics.Stopwatch.StartNew();
              int[] sHeap = hs.HeapSortS(small,small.Length);
             var elapsedMs = watch.ElapsedMilliseconds;
               times.Add(elapsedMs);*/

            /* var watch = System.Diagnostics.Stopwatch.StartNew();
              int[] mHeap = hs.HeapSortS(medium,medium.Length);
              var elapsedMs = watch.ElapsedMilliseconds;
              times.Add(elapsedMs);*/

            /* var watch = System.Diagnostics.Stopwatch.StartNew();
              int[] lHeap = hs.HeapSortS(large,large.Length);
              var elapsedMs = watch.ElapsedMilliseconds;
              times.Add(elapsedMs);*/

            /* var watch = System.Diagnostics.Stopwatch.StartNew();
              int[] sMerge = ms.Sort(small);
              var elapsedMs = watch.ElapsedMilliseconds;
              times.Add(elapsedMs);*/


            /* var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] mMerge = ms.Sort(medium);
             var elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);*/

            /* var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] lMerge = ms.Sort(large);
             var elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);*/

            /* var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] sQuick = qs.Sort(small);
             var elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);*/

            /* var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] mQuick = qs.Sort(medium);
             var elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);*/

            /* var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] lQuick = qs.Sort(large);
             var elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);*/

            /*var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] sShell = ss.Sort(small);
            var  elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);*/

            /*var watch = System.Diagnostics.Stopwatch.StartNew();
            int[] mShell = ss.Sort(medium);
            var elapsedMs = watch.ElapsedMilliseconds;
            times.Add(elapsedMs);*/

            /*var watch = System.Diagnostics.Stopwatch.StartNew();
            int[] lShell = ss.Sort(large);
            var elapsedMs = watch.ElapsedMilliseconds;
            times.Add(elapsedMs);*/

           /* var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] sBubble = bs.Sort(small);
             var elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);*/
           
           /* var watch = System.Diagnostics.Stopwatch.StartNew();
             int[] mBubble = bs.Sort(medium);
             var elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);
            int k = 0;*/
             var  watch = System.Diagnostics.Stopwatch.StartNew();
             int[] lBubble = bs.Sort(large);
             var  elapsedMs = watch.ElapsedMilliseconds;
             times.Add(elapsedMs);

        }
        private static int[] GenerateRandomNumbers(int count)
        {
            int[] numbers = new int[count];
            Random rand = new Random();
            for (int i = 0; i < count; i++)
            {
                numbers[i] = rand.Next(1,count/2);
            }
            return numbers;
        }

    }

}
