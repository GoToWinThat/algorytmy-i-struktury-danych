﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortowanie
{
    public class ShellSort
    {
        public int[] Sort(int[] data)
        {
            int h, i, j, temp;
            for ( h = data.Length / 2; h > 0; h /= 2)
            {
                for ( i = h; i < data.Length; i++)
                {
                    temp = data[i];
                    for (j = i; j >= h;  j -= h)
                    {
                        data[j] = data[j - h];
                        if(data[j - h] < temp)
                        {
                            break;
                        }
                    }
                    data[j] = temp;
                }
            }
            return data;
        }
    }
}
