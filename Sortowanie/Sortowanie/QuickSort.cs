﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortowanie
{
    public class QuickSort
    {
        private int position { get; set; }
        public int[] Sort(int[] data)
        {
            QSort(data, 0, data.Length-1);
            return data;
        }
        private void QSort(int[] data, int start, int end)
        {
            if (start < end)
            {
                position = Partition(data, start, end);
                QSort(data, start, position - 1);
                QSort(data, position + 1, end);
            }
        }
        //Kopiowanie do podtablicy
        private int Partition(int[] data, int start, int end)
        {
            int temp;
            int i = start -1;

            for (int j = start; j <= end - 1; j++)
            {
                if (data[j] <= data[end])
                {
                    //Podmiana
                    i++;
                    temp = data[i];
                    data[i] = data[j];
                    data[j] = temp;
                }
            }
            //Podmiana 
            temp = data[i + 1];
            data[i + 1] = data[end];
            data[end] = temp;
            return i + 1;
        }
    }
}
