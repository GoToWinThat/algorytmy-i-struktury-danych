﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortowanie
{
    //Zmienne oraz algorytm na podstawie ksiązki "Wprowadzenie do algorytmów"
    class MergeSort
    {
        public int[] Sort(int[]data)
        {
            Split(data, 0, data.Length-1);
            return data;
        }
        private void Split(int[] A,int p,int r)
        {
            if(p<r)
            {
                int q = (p + r) / 2;
                Split(A, p, q);
                Split(A, q + 1, r);
                Merge(A,p,q,r);
            }
        }
        private void Merge(int[]A, int p,int q,int r)
        {
            int i = 0;
            int n1 = q - p + 1;
            int n2 = r - q;
            int[] L = new int[n1+1];
            int[] R = new int[n2+1];
            for (i = 0; i < n1; i++)
            {
                L[i] = A[p + i];
            }
            for (i = 0; i < n2; i++)
            { 
                R[i] = A[q+i+1];
            }
            L[n1] = int.MaxValue;
            R[n2] = int.MaxValue;
            i = 0;
            int j = 0;
            for (int k = p; k < r; k++)
            {
                if (L[i] <= R[j])
                {
                    A[k] = L[i];
                    i = i + 1;
                }
                else 
                {
                    A[k] = R[j];
                    j = j + 1;
                }
            }
        }
    }
}
