﻿

namespace Trees
{
    public enum TraversalEnum
    {
        PREORDER,
        INORDER,
        POSTORDER
    }
}