﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace GraphAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            ///////////////////////////////////////////////////////////////////////////////////////////
            //Dijkstra Algorithm:
            /* List<List<int>> ver = new List<List<int>>();

             ver.Add(
                  new List<int> { 0, 4, 0, 0, 0, 0, 0, 8, 0 }
                  );
             ver.Add(
                 new List<int> { 4, 0, 8, 0, 0, 0, 0, 11, 0 }
                 );
             ver.Add(
                 new List<int> { 0, 8, 0, 7, 0, 4, 0, 0, 2 }
                 );
             ver.Add(
                 new List<int> { 0, 0, 7, 0, 9, 14, 0, 0, 0 }
                 );
             ver.Add(
                 new List<int> { 0, 0, 0, 9, 0, 10, 0, 0, 0 }
                 );
             ver.Add(
                 new List<int> { 0, 0, 4, 14, 10, 0, 2, 0, 0 }
                 );
             ver.Add(
                 new List<int> { 0, 0, 0, 0, 0, 2, 0, 1, 6 }
                 );
             ver.Add(
                 new List<int> { 8, 11, 0, 0, 0, 0, 1, 0, 7 }
                 );
             ver.Add(
                  new List<int> { 0, 0, 2, 0, 0, 0, 6, 7, 0 }
                  );
             Console.WriteLine("Algortym Dijkstry: Wylicza ścieżki od punktu początkowego do reszty punktów." +
                 "Na potrzeby programu program ukazuje dlugosci sciezek do kazdego z tych punktow oraz " +
                 "sciezke poprzednikow od punktu podanego jako argument");
             Dijkstra dijkstra = new Dijkstra(ver,0,3);
             dijkstra.FindPath();*/
            ///////////////////////////////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////////////////////////////////////
            //Floyd Algorithm
            /* List<List<int>> ver = new List<List<int>>();
             ver.Add(
                 new List<int> { 0, 8, 5 }
                 );
             ver.Add(
                new List<int> { 2, 0, int.MaxValue }
                );
             ver.Add(
                new List<int> { int.MaxValue, 1, 0 }
                );
            Console.WriteLine("Algortym Floyda: Wylicza ścieżki od każdego wierzchołka do każdego" +
                "dlatego wynikiem jest macierz obrazujaca liste długości ścieżek dla każdego wierzchołka");
            Floyd test = new Floyd(ver);
            test.FindPath();*/
            ///////////////////////////////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////////////////////////////////////
            //BFS algorithm
           /* List<List<int>> ver = new List<List<int>>();
            ver.Add(
                new List<int> { 0, 1, 1, 1, 0, 0, 0, 0 }
                );
            ver.Add(
               new List<int> { 1, 0, 1, 0, 0, 1, 0, 1 }
               );
            ver.Add(
               new List<int> { 1, 1, 0, 1, 1, 0, 0, 1 }
               );
            ver.Add(
               new List<int> { 1, 0, 1, 0, 1, 0, 0, 0 }
               );
            ver.Add(
               new List<int> { 0, 0, 1, 1, 0, 0, 1, 1 }
               );
            ver.Add(
               new List<int> { 0, 1, 0, 0, 0, 0, 1, 1 }
               );
            ver.Add(
               new List<int> { 0, 0, 0, 0, 1, 1, 0, 1 }
               );
            ver.Add(
               new List<int> { 0, 1, 1, 0, 1, 1, 1, 0 }
               );
            Console.WriteLine("Algortym BFS: Algortym przeszukiwania wszerz od wierzcholka poczatkowego" +
                "dociera do kazdego wierzcholka w grafie oraz zapisuje jego poprzednikow");
            BFS bfs = new BFS(ver, 3, 6);
            bfs.FindPath();*/
            ///////////////////////////////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////////////////////////////////////
            List<List<int>> ver = new List<List<int>>();

              ver.Add(
                  new List<int> { 0, 1, 1, 1, 0, 0, 0, 0 }
                  );
              ver.Add(
                 new List<int> { 1, 0, 1, 0, 0, 1, 0, 1 }
                 );
              ver.Add(
                 new List<int> { 1, 1, 0, 1, 1, 0, 0, 1 }
                 );
              ver.Add(
                 new List<int> { 1, 0, 1, 0, 1, 0, 0, 0 }
                 );
              ver.Add(
                 new List<int> { 0, 0, 1, 1, 0, 0, 1, 1 }
                 );
              ver.Add(
                 new List<int> { 0, 1, 0, 0, 0, 0, 1, 1 }
                 );
              ver.Add(
                 new List<int> { 0, 0, 0, 0, 1, 1, 0, 1 }
                 );
              ver.Add(
                 new List<int> { 0, 1, 1, 0, 1, 1, 1, 0 }
                 );
            Console.WriteLine("Algortym DFS: Algortym przeszukiwania w głąb zaczyna od pierwszego wierzchołka, bada wszystkie krawedzie" +
                "a nastepnie wraca do wierzcholka poczatkowego.");
            DFS TEST = new DFS(ver, 0, 6);
              TEST.FindPath();
            ////////////////////////////////////////////////////////////////////////
            Console.ReadKey();
        }
    }
}

