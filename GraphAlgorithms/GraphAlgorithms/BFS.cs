﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphAlgorithms
{
    class BFS
    {
        private List<Vertex> vertices = new List<Vertex>();
        private Queue<Vertex> vertexQueue = new Queue<Vertex>();
        private int SourceVer { get; set; }
        private int DestinationVer { get; set; }
        private class Vertex
        {
            public List<int> Connections;
            public int label { get; set; }
            public Status status { get; set; }
            public int lenght { get; set; }
            public Vertex previous { get; set; }
            public Vertex(List<int> con)
            {
                Connections = con;
            }
        }
        private enum Status
        {
            NotTaken=0,
            Taken=1,
        }
        public BFS(List<List<int>> ver, int source, int dest)
        {
            for (int i = 0; i < ver.Count; i++)
            {
                Vertex v = new Vertex(ver[i]);
                v.label = i;
                if(i==source)
                {
                    v.lenght = 0;
                    v.status = Status.Taken;
                }
                else
                {
                    v.lenght = int.MaxValue;
                    v.status = Status.NotTaken;
                }
                v.previous = null;
                vertices.Add(v);
            }
            SourceVer = source;
            DestinationVer = dest;

            vertexQueue.Enqueue(vertices[SourceVer]);
        }
        public void FindPath()
        {
            while(vertexQueue.Count>0)
            {
                Vertex ver = vertexQueue.Dequeue();

                for(int i=0; i<ver.Connections.Count(); i++)
                {
                    if(ver.Connections[i]==1)
                    {
                        if(vertices[i].status==Status.NotTaken)
                        {
                            vertices[i].status = Status.Taken;
                            vertices[i].lenght = ver.lenght + 1;
                            vertices[i].previous = vertices[ver.label];
                            vertices[i].status = Status.Taken;
                            vertexQueue.Enqueue(vertices[i]);
                        }
                    }
                }
            }
            ShowSolution();
        }
        private void ShowSolution()
        {
            int pick = DestinationVer;
            if (pick == SourceVer)
            {
                Console.WriteLine("Droga i cel takie same");
                return;
            }
            Console.WriteLine($"Celem jest {DestinationVer}");
            Console.WriteLine("Poprzednicy: ");
            while (pick != SourceVer)
            {
                Console.Write(vertices[pick].previous.label);
                pick = vertices[pick].previous.label;
                if (pick != SourceVer)
                {
                    Console.Write(" -> ");
                }
            }
        }
    }
}
