﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphAlgorithms
{
    public class Dijkstra
    {
        private List<Vertex> vertices = new List<Vertex>();
        private int SourceVer { get; set; }
        private int DestinationVer { get; set; }
        List<KeyVal<int, bool>> DistanceFromSource { get; set; }
        public Dijkstra (List<List<int>> ver, int source, int dest)
        {
           for(int i=0; i<ver.Count; i++)
           {
                Vertex v = new Vertex(ver[i]);
                v.label = i;
                if(i==source)
                {
                    v.pathWeight = 0;
                }
                else
                {
                    v.pathWeight = int.MaxValue;
                }
                v.previous = null;
                vertices.Add(v);
           }
            SourceVer = source;
            DestinationVer = dest;
        }
        public void FindPath()
        {
            DistanceFromSource = SetDistance(vertices.Count(), SourceVer);

            for(int i=0; i<vertices.Count-1;i++)
            {
                //Wytłumaczenie LINQ:
                // Findex - szuka indeksu w liście na bazie dwóch warunków
                // a => a.isTaken == false - element listy, który jest obiektem klasy KeyVal musi mieć wartość false(wierzchołek nie zabrany ze zbioru)
                // MINIMUM = DistanceFromSource.Where(v => v.isTaken == false).Min(m => m.path) - znajduje wartosc minimalna z listy dla parametru isTaken==false
                // a.path==MINIMUM
                //LINQ nie pozwala na uzycie metod findindex lub index of po uzyciu "where" 
                //dlatego najpierw wyszukujemy wartosci minimalnej z warunek na false a nastepnie uzywamy wyszukanej wartosci w warunku metody findindex
                int minIndex = DistanceFromSource.FindIndex(
                    a => a.isTaken == false && 
                    a.path == (
                    DistanceFromSource.Where(v => v.isTaken == false).Min(m => m.path))
                    );

                DistanceFromSource[minIndex].isTaken = true;

                Relax(DistanceFromSource, minIndex);

            }
            ShowAllPaths();
            ShowSolution();
        }
        void Relax(List<KeyVal<int, bool>> dist, int index)
        {
            for(int v=0; v< vertices.Count; v++)
            {
                if(v==index)
                {
                    continue;
                }
                else if(dist[v].isTaken==false)
                {
                    if(vertices[index].Lenghts[v]!=0)
                    {
                        if ((dist[index].path + vertices[index].Lenghts[v]) < dist[v].path)
                        {
                            dist[v].path = (dist[index].path + vertices[index].Lenghts[v]);
                            vertices[v].pathWeight = dist[v].path;
                            vertices[v].previous = vertices[index];
                        }
                    }
                }
            }
        }
        private List<KeyVal<int, bool>> SetDistance(int count,int source)
        {
            List<KeyVal<int, bool>> result = new List<KeyVal<int, bool>>();
            for (int i = 0; i < count; i++)
            {
                if(i==source)
                {
                    result.Add(
                        new KeyVal<int, bool>(0,false)
                        );
                }
                else
                {
                    result.Add(
                        new KeyVal<int, bool>(int.MaxValue, false)
                        );
                }
            }
            return result;
        }
        private void ShowSolution()
        {
            int pick = DestinationVer;
            if(pick==SourceVer)
            {
                Console.WriteLine("Droga i cel takie same");
                return;
            }
            Console.WriteLine($"Celem jest {DestinationVer}");
            Console.WriteLine("Poprzednicy wraz z długością ścieżki: ");
            while (pick != SourceVer)
            {
                Console.Write(vertices[pick].previous.label);
                Console.Write(" -> ");
                pick = vertices[pick].previous.label;

            }
            Console.Write($"Długość - {vertices[DestinationVer].pathWeight}");
        }
        private void ShowAllPaths()
        {
            Console.WriteLine("Wszystkie drogi (funkcjonalność na potrzeby sprawdzającego kod. Do odkomentowanie w klasie Dikjstra -> metoda FindPath ->linia 59)");
            foreach(var el in vertices)
            {
                Console.WriteLine($"{el.label}: {el.pathWeight}");
            }
        }
        private class KeyVal<Key, Val>
        {
            public Key path { get; set; }
            public Val isTaken { get; set; }

            public KeyVal(Key key, Val val)
            {
                this.path = key;
                this.isTaken = val;
            }
        }
        private class Vertex
        {
            public List<int> Lenghts;
            public int label { get; set; }
            public int pathWeight { get; set; }
            public Vertex previous { get; set; }
            public Vertex(List<int> len)
            {
                Lenghts = len;
            }
        }
    }
}
