﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphAlgorithms
{
    class DFS
    {
        private List<Vertex> vertices = new List<Vertex>();
        private List<int> travelsalOrder = new List<int>();
        private int SourceVer { get; set; }
        private int DestinationVer { get; set; }
        private int time = 0;
        private class Vertex
        {
            public List<int> Connections;
            public int label { get; set; }
            public Status status { get; set; }
            public int dTime { get; set; }
            public int fTime { get; set; }
            public Vertex previous { get; set; }
            public Vertex(List<int> con)
            {
                Connections = con;
            }
        }
        private enum Status
        {
            NotTaken = 0,
            Taken = 1,
            Saved=2
        }
        public DFS(List<List<int>> ver, int source, int dest)
        {
            for (int i = 0; i < ver.Count; i++)
            {
                Vertex v = new Vertex(ver[i]);
                v.label = i;
                v.status = Status.NotTaken;
                v.dTime = 0;
                v.fTime = 0;
                v.previous = null;
                vertices.Add(v);
            }
            SourceVer = source;
            DestinationVer = dest;
        }
        public void FindPath()
        {
            for(int i=0; i<vertices.Count(); i++)
            {
                if(vertices[i].status==Status.NotTaken)
                {
                    DFS_Visit(i);
                }
            }
            ShowAllVertex();
            ShowPath();
        }
        private void DFS_Visit(int index)
        {
            time++;
            vertices[index].dTime = time;
            vertices[index].status = Status.Taken;
            travelsalOrder.Add(index);
            for (int i=0; i<vertices.Count();i++)
            {
                if(vertices[index].Connections[i]==1)
                {
                    if(vertices[i].status == Status.NotTaken)
                    {
                        vertices[i].previous = vertices[index];
                        DFS_Visit(i);
                    }
                }
            }
            vertices[index].status = Status.Saved;
            time++;
            vertices[index].fTime = time;
        }

        private void ShowAllVertex()
        {
            foreach(var v in vertices)
            {
                Console.WriteLine($"Numer:{v.label}. Czas odkrycia: {v.dTime}. Czas zakończenia: {v.fTime}");
            }
        }
        private void ShowPath()
        {
            int source = travelsalOrder.IndexOf(SourceVer);
            int dest = travelsalOrder.IndexOf(DestinationVer);
            if (source == dest)
            {
                Console.WriteLine("Droga i cel takie same");
                return;
            }
            Console.WriteLine($"Celem jest {DestinationVer}");
            Console.WriteLine("Droga ode źródła do celu:");
            if(source<dest)
            {
                for(int i=source;i<=dest;i++)
                {
                    Console.WriteLine(travelsalOrder[i]);
                }
            }
            else 
            {
                for (int i = source; i >= dest; i--)
                {
                    Console.WriteLine(travelsalOrder[i]);
                }
            }
        }
    }
}
