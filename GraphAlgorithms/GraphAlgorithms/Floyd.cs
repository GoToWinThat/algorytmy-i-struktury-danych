﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphAlgorithms
{
    public  class Floyd
    {
        private List<Vertex> vertices = new List<Vertex>();
        public Floyd(List<List<int>> ver)
        {
            for (int i = 0; i < ver.Count; i++)
            {
                Vertex v = new Vertex(ver[i]);
                v.label = i;
                v.previous = null;
                vertices.Add(v);
            }
        }
        public void FindPath()
        { 
            for (int k = 0; k < vertices.Count(); k++)
            {
                for (int i = 0; i < vertices.Count(); i++)
                {
                    for (int j = 0; j < vertices.Count(); j++)
                    {
                        int one = vertices[i].Lenghts[k];
                        int two = vertices[k].Lenghts[j];
                        int three = vertices[i].Lenghts[j];
                        if(one!= int.MaxValue && two!= int.MaxValue)
                        {
                            if (one + two < three)
                            {
                                vertices[i].Lenghts[j] = one + two;
                            }
                        }
                    }
                }
            }
            ShowAll();
        }
        private void ShowAll()
        {
            for(int i=0;i<vertices.Count;i++)
            {
                for(int k=0; k<vertices[i].Lenghts.Count();k++)
                {
                    if(vertices[i].Lenghts[k]==int.MaxValue)
                    {
                        Console.WriteLine("INF");
                    }
                    else 
                    { 
                        Console.Write(vertices[i].Lenghts[k]); 
                    }
                    Console.Write("   ");
                }
                Console.WriteLine();
            }
        }
        private class Vertex
        {
            public List<int> Lenghts;
            public int label { get; set; }
            public Vertex previous { get; set; }

            public Vertex(List<int> len)
            {
                Lenghts = len;
            }
        }
    }
}
